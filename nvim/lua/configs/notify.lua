require('notify').setup {
    background_colour = '#222222',
    max_width = 32,
    render = "wrapped-compact"
}

-- This should load automatically, accroding to doc...
require('telescope').load_extension('notify')

vim.api.nvim_create_user_command('Notifications', function()
    require('telescope').extensions.notify.notify()
end, {bang = true})

vim.keymap.set(
    'n', '<leader>nc', function()
        require('telescope').extensions.notify.notify()
    end, {silent = true, noremap = true}
)

vim.notify = require("notify")

