require('sniprun').setup({
    display = { 
        -- "Terminal" 
        "NvimNotify"
    }
})

vim.keymap.set(
    {'v'}, '<F4>', 
    '<Plug>SnipRun', 
    {silent = true, noremap = true}
)

