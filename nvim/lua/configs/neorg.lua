require('neorg').setup {
    load = {
        ["core.defaults"] = {},
        ["core.concealer"] = {
            config = {
                icon_preset = 'varied',
                icons = {
                    todo = {
                        done = {
                            icon = '✔'
                        },
                        undone = {
                            icon = ' '
                        }
                    }
                }
            }
        },
        ["core.dirman"] = {
            config = {
                workspaces = {
                    cloud = "~/Nextcloud/Notes"
                },
                autochdir = false,
                index = "index.norg"
            }
        },
        ["core.qol.toc"] = {},
        ["core.completion"] = {
            config = {
                engine = "nvim-cmp"
            }
        },
        ["core.integrations.nvim-cmp"] = {},
        ["core.export"] = {},
        ["core.export.markdown"] = {
            config = {
                extensions = "all"
            }
        },
        ["core.integrations.telescope"] = {}
    }
}

