-- Vanilla vim settings.
require('configs.vanilla')

-- Load plugins.
require('plugins')

-- Project configuration support.
require('configs.project')

