local fterm = require('FTerm')

fterm.setup({
    border = 'rounded',
})

for i = 7, 9 do
    local term = fterm:new({
        ft = 'fterm_'..i
    })
    vim.keymap.set(
        {'n', 'i', 't'}, string.format('<F%d>', i), function()
            term:toggle()
        end, {silent = true, noremap = true}
    )
end

local gitui = fterm:new({
    ft = 'fterm_gitui',
    cmd = "gitui",
})

vim.api.nvim_create_user_command('Gitui', function()
    gitui:toggle()
end, {bang=true})

