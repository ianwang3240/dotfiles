local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")

local screenshot = string.format(
    "flameshot full -p %s/Pictures/Screenshots", os.getenv("HOME")
)

local screenshot_capture = string.format(
    "flameshot gui -p %s/Pictures/Screenshots", os.getenv("HOME")
)

function callback(stdout, stderr, exitreason, exitcode)
    if exitcode == 0 then
        naughty.notify({ 
            preset = naughty.config.presets.normal,
            title = "Screenshot Saved.",
        })
    end
end

return {
    hotkeys = gears.table.join(
        awful.key(
            {}, "Print",
            function()
                awful.spawn(screenshot)
            end, { 
                description = "take a screenshot",
                group = "screenshot"
            }
        ),
        awful.key(
            { modkey, }, "Print",
            function()
                awful.spawn(screenshot_capture) 
            end, { 
                description = "take a screenshot with capture tool",
                group = "screenshot"
            }
        )
    )
}
