#!/bin/bash

function handle {
    if [[ ${1:0:12} == "monitoradded" ]]; then
        monitor=${1:14}
        profile=~/.config/hypr/scripts/monitor/$monitor

        if [[ -f $profile ]]; then
            source $profile
        fi
    fi
}

socat - UNIX-CONNECT:/tmp/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock | while read line; do 
    handle $line
done

