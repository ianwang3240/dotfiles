vim.g.beacon_ignore_filetypes = {'NvimTree', 'dashboard'}

vim.api.nvim_set_hl(0, 'Beacon', {
    bg='white', fg='None'
})

