local awful = require("awful")
local gears = require("gears")

local hotkeys_popup = require("awful.hotkeys_popup")

local logout_popup = require(
    "awesome-wm-widgets.logout-popup-widget.logout-popup"
)

local apps = require("applications")
 
return gears.table.join(
    awful.key(
        { modkey, "Shift" }, "/",      
        hotkeys_popup.show_help,
        {description="show help", group="awesome"}
    ),

    awful.key(
        { modkey, "Shift" }, "r", 
        awesome.restart,
        {description = "reload awesome", group = "awesome"}
    ),

    awful.key(
        { modkey, "Shift"   }, "q",
        function()
            logout_popup.launch{
                onlock = function()
                    -- lock
                    awful.spawn(apps.default.screen_locker)
                end,
                onreboot = function()
                    awful.spawn("loginctl reboot")
                end,
                onsuspend = function()
                    awful.spawn("loginctl suspend")
                end,
                onpoweroff = function()
                    awful.spawn("loginctl poweroff")
                end
            }
        end,
        {description = "logout", group = "awesome"}    
    ),
    
    awful.key({modkey}, "/",
        function ()
            awful.prompt.run {
                prompt       = "<b>Awesome   </b>",
                textbox      = awful.screen.focused().mypromptbox.widget,
                exe_callback = awful.util.eval,
                history_path = awful.util.get_cache_dir() .. "/history_eval"
            }
        end,
        {description = "lua execute prompt", group = "awesome"}
    )
)

