local setup = function(M, opts)
    local headers = {}

    -- Read config
    headers.headers = opts['headers'] or {}
    headers.prefixes = opts['prefixes'] or {}

    -- Find header with name.
    headers.header = function(name)
        return headers.headers[name]
    end

    -- Find prefix using file extension.
    headers.prefix = function(extension) 
        for pre, exts in pairs(headers.prefixes) do
            for _, ext in ipairs(exts) do
                if ext == extension then
                    return pre
                end
            end
        end
        
        -- Use # as default since most config file use this as comment.
        return '# '
    end

    M.profile.headers = headers
end

return {
    setup = setup
}

