local db = require("alpha.themes.dashboard")

-- Highlight group
vim.api.nvim_set_hl(0, 'dashboard.header', {
    fg = 'None'
})
vim.api.nvim_set_hl(0, 'dashboard.footer', {
    fg = 'lime'
})

-- Header.
local headers = require('configs.alpha.headers')
math.randomseed(os.time())

-- Some important location
local notes = '~/Nextcloud/Notes'
local nvim_config = '~/.config/nvim'

-- plugin count
local count = #vim.tbl_keys(require('lazy').plugins())

-- Help function for button creation.
local function button(label, on_press, shortcut)
    if shortcut then
        vim.api.nvim_create_autocmd("User", {
            pattern = "AlphaReady",
            callback = function(args)
                vim.keymap.set("n", shortcut, on_press, {
                    buffer = 0,
                    noremap = true, 
                    silent = true, 
                    nowait = true 
                })
            end
        })
    end

    return {
        type = "button",
        val = label,
        on_press = on_press,
        opts = {
            position = "center",
            shortcut = shortcut or " ",
            cursor = 3,
            width = 50,
            align_shortcut = "right"
        }
    }
end

db.section.header.val = headers[
    math.random(#headers)
]
db.section.header.opts.hl = "dashboard.header"

db.section.buttons.val = {
    button("  New file", function()
        vim.cmd("enew")
    end, "i"),
    button("  Open Recent", function()
        require("telescope.builtin").oldfiles()
    end, "h"),
    button("ﴬ  Notes", function()
        vim.cmd(string.format("cd %s | edit index.norg", notes))
    end, "n"),
    button("  Find Files", function()
        require("telescope.builtin").find_files()
    end, "f"),
    button("  Nvim Config", function()
        vim.cmd(string.format('cd %s | edit init.lua', nvim_config))
    end, "c"),
    button("󰅚  Quit", function()
        vim.cmd("qa")
    end, "q"),
}

db.section.footer.val = ' '.. count .. ' plugins loaded'
db.section.footer.opts.hl = "dashboard.footer"

require("alpha").setup(db.config)

