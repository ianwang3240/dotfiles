require("gitsigns").setup({
    signs = {
        add = {
            text = '+',
        },
        delete = {
            text = '-',
        },
        change = {
            text = '~',
        },
        changedelete = {
            text = '~_'
        },
        untracked = {
            text = ''
        }
    },
    current_line_blame_opts = {
        delay = 100
    }
})

vim.api.nvim_set_hl(0, 'GitSignsAdd', {
    fg = '#00FF00'
})
vim.api.nvim_set_hl(0, 'GitSignsChange', {
    fg = '#00AAFF'
})
vim.api.nvim_set_hl(0, 'GitSignsDelete', {
    fg = 'red'
})
vim.api.nvim_set_hl(0, 'GitSignsChangeDelete', {
    fg = 'red'
})
vim.api.nvim_set_hl(0, 'GitSignsCurrentLineBlame', {
    fg = 'gray'
})

-- Diff view
vim.api.nvim_set_hl(0, 'DiffAdd', {
    bg = '#004400'
})
vim.api.nvim_set_hl(0, 'DiffChange', {
    bg = '#444400'
})
vim.api.nvim_set_hl(0, 'DiffDelete', {
    bg = '#440000'
})

-- WARN: Maybe this will affect others. Use with causion.
vim.api.nvim_set_hl(0, 'FoldColumn', {
    bg = 'None'
})

vim.api.nvim_create_user_command('Gitblame', function()
    require('gitsigns').toggle_current_line_blame()
end, {bang=true})

vim.api.nvim_create_user_command('Gitdiff', function()
    require('gitsigns').diffthis()
end, {bang=true})

