-- Jump to definition using split_cmd.
local function goto_definition(split_cmd)
    local util = vim.lsp.util
    local log = require("vim.lsp.log")
    local api = vim.api

    -- note, this handler style is for neovim 0.5.1/0.6, if on 0.5, call with function(_, method, result)
    local handler = function(_, result, ctx)
        if result == nil or vim.tbl_isempty(result) then
            local _ = log.info() and log.info(
                ctx.method, 
                "No location found"
            )
            return nil
        end

        if split_cmd then
            vim.cmd(split_cmd)
        end

        if vim.tbl_islist(result) then
            util.jump_to_location(result[1])

            if #result > 1 then
                util.set_qflist(
                    util.locations_to_items(result)
                )
                api.nvim_command("copen")
                api.nvim_command("wincmd p")
            end
        else
            util.jump_to_location(result)
        end
    end

    return handler
end

-- Use new buffer to jump.
vim.lsp.handlers["textDocument/definition"] 
    = goto_definition('e')
vim.lsp.handlers["textDocument/hover"]
    = vim.lsp.with(vim.lsp.handlers.hover, {focusable = false})
vim.lsp.handlers["textDocument/signature_help"]
    = vim.lsp.with(vim.lsp.handlers.signature_help, {focusable = false})

