function activated_lsp_services()
    local clients = vim.lsp.get_active_clients()
    local services = {}
    for _, client in ipairs(clients) do
        table.insert(services, client.name)
    end

    return services
end

require('lualine').setup {
    options = {
        icons_enabled = true,
        theme = 'ayu_mirage',
    },
    sections = {
        lualine_x = {
            function()
                return ' '..vim.fn['ime#mode']()
            end, 'encoding', 'fileformat', 'filetype'
        },
        lualine_c = {
            function()
                return 'פּ '..table.concat(
                    activated_lsp_services(), ' '
                )
            end, 'filename', "aerial"
        }
    },
    tabline = {
        lualine_a = {{
            'buffers', mode = 4
        }},
        lualine_z = {'tabs'},
    },
    extensions = {
        'nvim-tree'
    }
}
