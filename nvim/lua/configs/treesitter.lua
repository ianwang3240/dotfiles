require('nvim-treesitter.configs').setup {
    ensure_installed = {
        "c",
        "cpp",
        -- This requires tree-sitter-cli and nodejs
        -- "latex",
        "lua",
        "python",
        "rust",
        "make",
        "cmake",
        "objdump",
        "markdown",
        "markdown_inline",
        "norg",
    },
    highlight = {
        enable = true,
    },
    indent = {
        enable = true,
        -- Not work very well...
        -- disable = {'c', 'cpp'}
    }
    -- Indent fix.
    -- yati = { 
    --     enable = true 
    -- },
}

vim.opt.foldmethod = 'expr'
vim.opt.foldexpr = 'nvim_treesitter#foldexpr()'
vim.opt.foldenable = false

-- Use simpler fold text
vim.cmd[[
    function! FoldText()
        return getline(v:foldstart)
    endfunction

    set foldtext=FoldText()
]]

-- Enable folding for certain filetype.
vim.api.nvim_create_autocmd({'VimEnter', 'FileType'}, {
    pattern = {"norg", "*.norg"},
    callback = function(args)
        vim.schedule(function()
            vim.cmd("normal zM")
        end)
    end
})

