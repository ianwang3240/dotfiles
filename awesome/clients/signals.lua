local awful = require("awful")

client.connect_signal("property::urgent", function(c)
    if c.urgent then
        c:jump_to()
    end
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal(
        "request::activate", "mouse_enter", {
            raise = false
        }
    )
end)

