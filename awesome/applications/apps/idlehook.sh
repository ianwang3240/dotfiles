#!/bin/bash

# Run xidlehook.

killall xidlehook

LOCKER=$HOME/.config/awesome/applications/apps/i3lock-color.sh

xidlehook \
--not-when-fullscreen \
--not-when-audio \
--timer 600 "$LOCKER" "" &

