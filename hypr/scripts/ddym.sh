#!/bin/sh

# Run dym with fuzzel (dmenu mode).

if [ -z "$1" ]; then
    WORD="$(fuzzel -d)"
else
    WORD="$1"
fi

if [ ! -z $WORD ]; then
    dym -c $WORD | fuzzel -d -p "Did you mean... " | wl-copy -n
fi

