local db = require('dashboard')

vim.cmd("highlight dashboardHeader guifg=None")

-- Header.
local headers = require('configs.dashboard.headers')
math.randomseed(os.time())

-- Some important location
local notes = '~/Nextcloud/Notes'
local nvim_config = '~/.config/nvim'

-- plugin count
local count = #vim.tbl_keys(require('lazy').plugins())

db.setup {
    theme = 'doom',

    hide = {
        statusline = false,
        tabline = false
    },

    config = {
        header = headers[
            math.random(#headers)
        ],
        center = {
            {
                icon = '  ',
                desc = 'Open Recent                    ',
                action = 'Telescope oldfiles',
                shortcut = '           '
            }, {
                icon = 'ﴬ  ',
                desc = 'Notes                          ',
                shortcut = '           ',
                action = string.format(':cd %s | edit index.norg', notes)
            }, {
                icon = '  ',
                desc = 'New File                       ',
                shortcut = '           ',
                action = ':enew'
            }, {
                icon = '  ',
                desc = 'Find Files                     ',
                shortcut = '<leader> ff',
                action = 'Telescope find_files'
            }, {
                icon = '  ',
                desc = 'Nvim Config                    ',
                shortcut = '           ',
                action = string.format(':cd %s | edit init.lua', nvim_config)
            }
        },
        footer = {
            '', '', ' NeoVim loaded '.. count .. ' plugins'
        }
    }
}
