require('trouble').setup {
    mode = 'quickfix',
    auto_preview = false,
    action_keys = {
        cancel = {}
    }
}
