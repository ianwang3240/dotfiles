#!/bin/bash

# Close menu and reset keybinding before proceed.
hyprctl dispatch submap reset
eww close-all

if [ "$1" == "poweroff" ]; then
    loginctl poweroff
elif [ "$1" == "reboot" ]; then
    loginctl reboot
elif [ "$1" == "logout" ]; then
    hyprctl dispatch exit
elif [ "$1" == "sleep" ]; then
    sleep 1 && killall swayidle -s SIGUSR1
else
    echo "Unknow operation!"
fi

