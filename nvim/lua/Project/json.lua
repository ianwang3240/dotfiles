local json = {}

json.import = function(path)
    local project = require('Project')

    local file = io.open(path, 'r')
    local options = vim.json.decode(file:read('*a'))

    for opt, val in pairs(options) do
        if opt == "build" then
            for build_opt, build_val in pairs(val) do
                if build_opt == "system" then
                    project.builder.set_build_system(build_val)
                elseif build_opt == "target" then
                    project.builder.set_build_target(build_val)
                end
            end
        elseif opt == "profile" then
            project.profile.set_profile(
                val
            )
        end
    end

    file:close()
end

json.export = function(path)
    local project = require('Project')

    local file = io.open(path, 'w')
    local options = vim.json.encode({
        build = {
            system = project.builder.build_system,
            target = project.builder.target
        },
        profile = project.profile.fields
    })

    file:write(options)
    file:close()
end

return {
    setup = function(M, opts)
        M.json = json

        M.register_command('json', {
            subcommands = {
                export = {
                    execute = function(fargs)
                        json.export(vim.fn.expand(table.concat(fargs)))
                    end
                },
                import = {
                    execute = function(fargs)
                        json.import(vim.fn.expand(table.concat(fargs)))
                    end
                }
            }
        })
    end
}
