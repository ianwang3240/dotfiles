#!/bin/sh

# Run i3lock-color with parameters.

BLANK='#00000000'
CLEAR='#00000000'
DEFAULT='#00aaff44'
KEYPRESS='#00aaffff'
BACKSPACE='#ff0000ff'
TEXT='#00aaffff'
WRONG='#ff0000ff'
VERIFYING='#00aaffff'
IMAGE="$HOME/.config/awesome/themes/sidonia/background.png"

i3lock \
--nofork                     \
\
--color=$CLEAR               \
--insidever-color=$CLEAR     \
--ringver-color=$VERIFYING   \
\
--insidewrong-color=$CLEAR   \
--ringwrong-color=$WRONG     \
\
--inside-color=$BLANK        \
--ring-color=$DEFAULT        \
--line-color=$BLANK          \
--separator-color=$DEFAULT   \
\
--verif-color=$TEXT          \
--wrong-color=$TEXT          \
--time-color=$TEXT           \
--date-color=$TEXT           \
--keyhl-color=$KEYPRESS      \
--bshl-color=$BACKSPACE      \
\
--blur=5                     \
--radius 150                 \
--no-modkey-text             \
--clock                      \
--indicator                  \
--wrong-text="Access Denied" \
--verif-text="Verifying..."  \
--noinput-text=""            \
--time-str="%H:%M:%S"        \
--date-str="%A, %m %Y"       \
--pass-media-keys            \
$@
