local lspconfig = require('lspconfig')

-- Default configuraion.
local configs = require('configs.lsp.lsp-server-configs')

-- lspconfig.clangd.setup(configs)

lspconfig.ccls.setup(vim.tbl_extend('force', configs, {
    init_options = {
        compilationDatabaseDirectory = "build"
    }
}))

lspconfig.jedi_language_server.setup(configs)

lspconfig.texlab.setup(vim.tbl_extend('force', configs, {
    filetypes = {
        -- Include 'plaintex'
        'tex', 'bib', 'plaintex'
    },
    root_dir = function()
        return "."
    end,
    settings = {
        texlab = {
            rootDirectory = "."
        }
    }
}))

lspconfig.rust_analyzer.setup(configs)
