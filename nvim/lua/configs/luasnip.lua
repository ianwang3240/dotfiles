local luasnip = require("luasnip")

local snip = luasnip.snippet
local text = luasnip.text_node
local func = luasnip.function_node
local insert = luasnip.insert_node

-- C/C++
local function include_guard_name()
    return vim.fn.expand('%:t'):gsub("[%.%s]", "_")
end

luasnip.add_snippets(nil, {
    c = {
        snip({
            trig = "#guard",
            name = "Include Guard",
            dscr = "C/C++ header include guard."
        }, {
            text({"#ifndef "}), func(include_guard_name, {}),
            text({"", "#define "}), func(include_guard_name, {}),
            text({"", "", ""}), insert(0), text({"", ""}),
            text({"", "#endif /* "}), func(include_guard_name, {}), text(" */")
        }),
        snip({
            trig = "main",
            name = "Main",
            dscr = "C/C++ main function."
        }, {
            text({
                "int main(int argc, char* argv[]) {", 
                "    "}), insert(0), text({"", 
                "}"
            })
        })
    }
})

luasnip.filetype_extend('cpp', {'c'})
luasnip.filetype_extend('tpp', {'c', 'cpp'})

-- Python
luasnip.add_snippets(nil, {
    python = {
        snip({
            trig = "main",
            name = "Main",
            dscr = "Python main block"
        }, {
            text({
                "if __name__ == '__main__':", 
                "    "
            }), insert(0)
        }),
        snip({
            trig = "#!",
            name = "Python Interpreter",
            dscr = "Python interpreter and encoding header."
        }, {
            text({
                "#!/usr/bin/env python3",
                "#-*- coding: utf-8 -*-",
                ""
            }), insert(0)
        })
    }
})

-- Shell script.
luasnip.add_snippets(nil, {
    sh = {
        snip({
            trig = "#!",
            name = "Zsh Interpreter",
            dscr = "Zsh interpreter header."
        }, {
            text({
                "#!/bin/zsh",
                ""
            }), insert(0)
        }),
        snip({
            trig = "#!",
            name = "BASH Interpreter",
            dscr = "BASH interpreter header."
        }, {
            text({
                "#!/bin/bash",
                ""
            }), insert(0)
        })
    }
})

luasnip.filetype_extend('zsh', {'sh'})

