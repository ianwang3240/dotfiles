-- Add additional capabilities supported by nvim-cmp
capabilities = require(
    'configs.lsp.autocompletion'
)()

require("lsp_signature").setup({
    hint_prefix = " ",
})

vim.api.nvim_set_hl(0, 'LspSignatureActiveParameter', {
    fg = '#00ff00',
    bg = 'none'
})

-- Attach keymap to current buffer
-- after the language server attached.
local on_attach = function(client, bufnr)
    local buf_nore_silent = {
        buffer = bufnr, noremap = true, silent = true
    }

    vim.api.nvim_buf_set_option(
        bufnr, 
        'omnifunc', 
        'v:lua.vim.lsp.omnifunc'
    )
    
    vim.keymap.set(
        'n', 'gd', function()
            vim.lsp.buf.definition()
        end, buf_nore_silent
    )

    vim.keymap.set(
        'n', '<leader>rn', function()
            vim.lsp.buf.rename()
        end, buf_nore_silent
    )

    vim.keymap.set(
        'n', '<leader>ca', function()
            vim.lsp.buf.code_action()
        end, buf_nore_silent
    )

    -- Symbol highlight
    if client.server_capabilities.documentHighlightProvider then
        vim.api.nvim_set_hl(0, 'LspReferenceRead', {
            cterm = {
                bold = true,
            },
            ctermbg = 'red',
            bg = '#444444'
        })
        vim.api.nvim_set_hl(0, 'LspReferenceText', {
            cterm = {
                bold = true,
            },
            ctermbg = 'red',
            bg = '#444444'
        })
        vim.api.nvim_set_hl(0, 'LspReferenceWrite', {
            cterm = {
                bold = true,
            },
            ctermbg = 'red',
            bg = '#444444'
        })

        vim.api.nvim_create_augroup('lsp_document_highlight', {})
        vim.api.nvim_create_autocmd({'CursorHold'}, {
            group = 'lsp_document_highlight',
            buffer = bufnr,
            callback = function()
                vim.lsp.buf.document_highlight()
            end
        })
        vim.api.nvim_create_autocmd({'CursorMoved'}, {
            buffer = bufnr,
            group = 'lsp_document_highlight',
            callback = function()
                vim.lsp.buf.clear_references()
            end
        })
    end

    -- Show diagnostic on normal mode.
    vim.api.nvim_create_augroup("lsp_diagnostics", {})
    vim.api.nvim_create_autocmd({'CursorHold'}, {
        buffer = bufnr,
        group = "lsp_diagnostics",
        callback = function()
            vim.diagnostic.open_float(nil, {
                foucs=false, scope="line"
            })
        end
    })
    
    -- Use additional plugin instead.
    -- if client.server_capabilities.signatureHelpProvider then
    --     -- Show signature help on insert mode.
    --     vim.api.nvim_create_augroup("lsp_signature_help", {})
    --     vim.api.nvim_create_autocmd({'CursorHoldI'}, {
    --         group = "lsp_signature_help",
    --         buffer = bufnr,
    --         callback = function()
    --             vim.lsp.buf.signature_help()
    --         end
    --     })
    -- end

    -- if client.server_capabilities.hoverProvider then
    --     -- Show hover info on normal mode.
    --     vim.api.nvim_create_augroup("lsp_hover", {})
    --     vim.api.nvim_create_autocmd({'CursorHold'}, {
    --         group = "lsp_hover",
    --         buffer = bufnr,
    --         callback = function()
    --             if vim.api.nvim_get_current_line() ~= '' then
    --                 vim.lsp.buf.hover()
    --             end
    --         end
    --     })
    -- end
end

return {
    on_attach = on_attach,
    capabilities = capabilities,
}

