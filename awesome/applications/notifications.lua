local naughty = require("naughty")

-- Per-app based notification settings.

naughty.config.presets.chat = {
    timeout = 60
}
table.insert(naughty.dbus.config.mapping, {{
    appname = "nheko"
}, naughty.config.presets.chat})

naughty.config.presets.mail = {
    timeout = 60
}
table.insert(naughty.dbus.config.mapping, {{
    appname = "neomutt"
}, naughty.config.presets.mail})
