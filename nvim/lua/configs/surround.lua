require("nvim-surround").setup {
    keymaps = {
        normal = "<Leader>s",
        normal_cur = "<Leader>ss",
        visual = "<Leader>s",
        delete = "ds",
        change = "cs",
        change_cur = "css"
    },

    -- Change surrounds with following modifications:
    --  1. Do not add any additional space around target.
    --  2. Always remove additional space around target.
    surrounds = {
        ["("] = {
            add = { "(", ")" },
            delete = "^(. ?)().-( ?.)()$",
        },
        [")"] = {
            add = { "(", ")" },
            delete = "^(. ?)().-( ?.)()$",
        },
        ["{"] = {
            add = { "{", "}" },
            delete = "^(. ?)().-( ?.)()$",
        },
        ["}"] = {
            add = { "{", "}" },
            delete = "^(. ?)().-( ?.)()$",
        },
        ["<"] = {
            add = { "<", ">" },
            delete = "^(. ?)().-( ?.)()$",
        },
        [">"] = {
            add = { "<", ">" },
            delete = "^(. ?)().-( ?.)()$",
        },
        ["["] = {
            add = { "[", "]" },
            delete = "^(. ?)().-( ?.)()$",
        },
        ["]"] = {
            add = { "[", "]" },
            delete = "^(. ?)().-( ?.)()$",
        }
    }
}

