#!/bin/bash

# Output location
path=$HOME/Pictures/Screenshots

# Output file name
filename=screenshot_$(date "+%Y%m%d_%Hh%Mm%Ss%N").png

# Do screen capturing
if grim -g "$(slurp)" $path/$filename; then
    # Send notification
    action=$(dunstify "Screenshot" "A screenshot have saved to ${filename}" \
        --timeout=3000 \
        --action="default, Open" \
    )

    case "${action}" in
    "default")
        thunar $path/$filename
        ;;
    esac
fi

