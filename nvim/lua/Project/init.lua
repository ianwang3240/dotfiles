local M = {}

M.default_opts = {
    profile = {},

    -- How to launch built target
    run = function(cmd)
        -- Use external terminal emulator
        io.popen(string.format(
            'alacritty --hold --working-directory "%s" -e %s &> /dev/null &',
            vim.fn.getcwd(),
            cmd
        ))
        
        -- Use nvim floating terminal
        -- require('FTerm').scratch({ 
        --     auto_close = false,
        --     cmd = cmd
        -- })
    end,
    
    qflist = function(cmd)
        -- Use trouble
        -- require('trouble').toggle()

        -- Use default qflist
        vim.cmd('cwindow')
    end,

    -- Predefine build systems
    build_systems = {
        make = {
            configs = {
                -- Debug config.
                debug = {
                    -- Build command.
                    build = 'make ${target}',
                    -- Run command.
                    run = './${target}',
                },
                -- Release config.
                release = {
                    -- Build command.
                    build = 'make ${target}',
                    -- Run command.
                    run = './${target}',
                }
            },
            -- Default target. Can be overwrite.
            target = 'a.out'
        },

        cmake = {
            configs = {
                release = {
                    build = (
                        -- Default is compile executable to build folder.
                        'cmake -S . -B build '..
                        -- Export compile commands for LSP.
                        '   -DCMAKE_EXPORT_COMPILE_COMMANDS=1 && '.. 
                        'cmake --build build --target ${target}'
                    ),
                    run = 'build/${target}',
                },
                debug = {
                    build = (
                        'cmake -S . -B build '..
                        -- Set build type to Debug.
                        '    -DCMAKE_BUILD_TYPE=Debug '..
                        '    -DCMAKE_EXPORT_COMPILE_COMMANDS=1 && '..
                        'cmake --build build --target ${target}'
                    ),
                    -- Use lldb to debug.
                    run = (
                        'lldb -b '..
                        '   -o "process handle SIGINT -p ture"'..
                        '   -o run build/${target}'
                    )
                }
            },
            target = 'a.out',
            errorformat = {
                -- Adapt from default gcc.vim.
                [[%*[^"]"%f"%*\D%l:%c: %m]],
                [[%*[^"]"%f"%*\D%l: %m]],
                [["%f"%*\D%l:%c: %m]],
                [["%f"%*\D%l: %m]],
                [[%-G%f:%l: %trror: (Each undeclared identifier is reported only once]],
                [[%-G%f:%l: %trror: for each function it appears in.)]],
                [[%f:%l:%c: %trror: %m]],
                [[%f:%l:%c: %tarning: %m]],
                [[%f:%l:%c: %m]],
                [[%f:%l: %trror: %m]],
                [[%f:%l: %tarning: %m]],
                [[%f:%l: %m]],
                [[%f:\(%*[^\)]\): %m]],
                [["%f"\, line %l%*\D%c%*[^ ] %m]],
                [[%D%*\a[%*\d]: Entering directory %*[`']%f']],
                [[%X%*\a[%*\d]: Leaving directory %*[`']%f]],
                [[%D%*\a: Entering directory %*[`']%f]],
                [[%X%*\a: Leaving directory %*[`']%f]],
                [[%DMaking %*\a in %f]]
            }
        },

        python = {
            configs = {
                release = {
                    build = '',
                    run = 'python3 ${target}'
                },
                debug = {
                    build = '',
                    run = 'python3 -m pdb -c continue ${target}'
                }
            },
            target = '%'
        },

        cargo = {
            configs = {
                release = {
                    build = 'cargo build',
                    run = 'cargo run'
                },
                debug = {
                    build = 'cargo build',
                    run = 'cargo with rust-gdb -- run'
                }
            },
            target = '',
            -- Adapt from rust.vim:
            -- https://github.com/rust-lang/rust.vim
            errorformat = {
                [[%-G]],
                [[%-Gerror: aborting %.%#]],
                [[%-Gerror: Could not compile %.%#]],
                [[%Eerror: %m]],
                [[%Eerror[E%n]: %m]],
                [[%Wwarning: %m]],
                [[%Inote: %m]],
                [[%C %#--> %f:%l:%c]],
                [[%E  left:%m]],
                [[%C right:%m %f:%l:%c]],
                [[%Z]],
            }
        },

        latexmk = {
            configs = {
                debug = {
                    -- Use latexmk with xelatex.
                    build = 'latexmk -xelatex ${target}',
                    -- Instead of auto run,
                    -- it is better to open your own pdf viewer
                    -- to view changes.
                    -- Zathura will reload whenever file changed.
                    run = nil
                },
                release = {
                    build = 'latexmk -xelatex ${target}',
                    run = nil
                }
            },
            target = 'main.tex',
            -- Adapt from default tex.vim. 
            errorformat = {
                [[%E! LaTeX %trror: %m]],
                [[%E! %m]],
                [[%+WLaTeX %.%#Warning: %.%#line %l%.%#]],
                [[%+W%.%# at lines %l--%*\d]],
                [[%WLaTeX %.%#Warning: %m]],
                [[%Cl.%l %m]],
                [[%+C  %m.]],
                [[%+C%.%#-%.%#]],
                [[%+C%.%#[]%.%#]],
                [[%+C[]%.%#]],
                [[%+C%.%#%[{}\]%.%#]],
                [[%+C<%.%#>%.%#]],
                [[%C  %m]],
                [[%-GSee the LaTeX%m]],
                [[%-GType  H <return>%m]],
                [[%-G ...%.%#]],
                [[%-G%.%# (C) %.%#]],
                [[%-G(see the transcript%.%#)]],
                [[%-G\s%#]],
                [[%+O(%*[^()])%r]],
                [[%+O%*[^()](%*[^()])%r]],
                [[%+P(%f%r]],
                [[%+P %\=(%f%r]],
                [[%+P%*[^()](%f%r]],
                [[%+P[%\d%[^()]%#(%f%r]],
                [[%+Q)%r]],
                [[%+Q%*[^()])%r]],
                [[%+Q[%\d%*[^()])%r]]
            }
        }
    },

    -- Predefined headers.
    headers = {
        -- Plain header with file name, user name and created date.
        plain = {
            '',
            ' ${file_name}',
            ' ${package_name}',
            '',
            ' Created by ${user_name} on ${year}/${month}/${day}.',
            '',
        },
        -- Header with copyright declaration.
        copyright = {
            '',
            ' ${file_name} ',
            ' ${package_name}',
            '',
            ' Created by ${user_name} on ${year}/${month}/${day}.',
            ' Copyright © ${year} ${user_name}. All rights reserved.',
            '',
        },
        -- Header with MIT license.
        MIT = {
            ' ${file_name}',
            ' ${package_name}',
            '',
            ' Copyright (c) 2020-${year} ${user_name}',
            '',
            ' Permission is hereby granted, free of charge, to any person obtaining',
            ' a copy of this software and associated documentation files (the',
            ' "Software"), to deal in the Software without restriction, including',
            ' without limitation the rights to use, copy, modify, merge, publish,',
            ' distribute, sublicense, and/or sell copies of the Software, and to',
            ' permit persons to whom the Software is furnished to do so, subject to',
            ' the following conditions:',
            '',
            ' The above copyright notice and this permission notice shall be',
            ' included in all copies or substantial portions of the Software.',
            '',
            ' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,',
            ' EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF',
            ' MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND',
            ' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE',
            ' LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION',
            ' OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION',
            ' WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.',
            '',
        },
        -- Header with GPLv3 license.
        GPL = {
            '',
            ' ${file_name}',
            ' Copyright (C) ${year} ${user_name}',
            '',
            ' This program is free software: you can redistribute it and/or modify',
            ' it under the terms of the GNU General Public License as published by',
            ' the Free Software Foundation, either version 3 of the License, or',
            ' (at your option) any later version.',
            '',
            ' This program is distributed in the hope that it will be useful,',
            ' but WITHOUT ANY WARRANTY; without even the implied warranty of',
            ' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the',
            ' GNU General Public License for more details.',
            '',
            ' You should have received a copy of the GNU General Public License',
            ' along with this program.  If not, see <http://www.gnu.org/licenses/>.',
            '',
        },
        -- Header with losser GPL license.
        LGPL = {
            ' ${file_name}',
            ' Copyright (C) ${year} ${user_name}',
            ' ${email}',
            ' ',
            ' This program is free software; you can redistribute it and/or',
            ' modify it under the terms of the GNU Lesser General Public',
            ' License as published by the Free Software Foundation; either',
            ' version 3 of the License, or (at your option) any later version.',
            ' ',
            ' This program is distributed in the hope that it will be useful,',
            ' but WITHOUT ANY WARRANTY; without even the implied warranty of',
            ' MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU',
            ' Lesser General Public License for more details.',
            ' ',
            ' You should have received a copy of the GNU Lesser General Public License',
            ' along with this program; if not, write to the Free Software Foundation,',
            ' Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.',
        },
    },

    -- Predefined prefixes.
    prefixes = {
        ['// '] = {
            -- C
            'c',
            'h',
            -- C++
            'cpp',
            'cxx',
            'hpp',
            'ipp',
            'hxx',
            -- Java
            'java',
            -- Rust
            'rc'
        },
        ['# '] = {
            -- Python
            'py',
            -- Shell script
            'sh'
        },
        ['-- '] = {
            -- Lua
            'lua'
        }
    }
}

M.setup = function(opts)
    local opts = vim.tbl_deep_extend(
        'force', M.default_opts, opts or {}
    )

    require('Project.profile').setup(M, opts)
    require('Project.build_system').setup(M, opts)
    require('Project.json').setup(M, opts)
end

M.commands = {
    subcommands = {
        -- Example:
        -- example = {
        --     subcommands = {
        --         sub = {
        --             execute = function(fargs)
        --                 ...
        --             end
        --             complete = function(lead, fargs)
        --                 ...
        --             end
        --         }
        --     }
        -- }
    }
}

M.register_command = function(command, opts)
    M.commands = vim.tbl_deep_extend('force', M.commands, {
        subcommands = {
            [command] = opts
        }
    })
end

vim.api.nvim_create_user_command(
    'Project',
    function(args)
        local fargs = args.fargs
        local node = M.commands

        while #fargs > 0 do
            local subcommand = fargs[1]
            local subcommands = node['subcommands']
            -- Dynamical subcommands
            if type(subcommands) == 'function' then
                subcommands = subcommands(fargs)
            end

            if subcommands == nil or subcommands[subcommand] == nil then
                -- If no further subcommand,
                -- we can execute command with remaining arguments now.
                break
            else
                -- Go down to next level
                node = subcommands[subcommand]
            end

            table.remove(fargs, 1)
        end
        
        -- Execute command with no arguments.
        if node['execute'] ~= nil then
            node.execute(fargs)
        else
            print("Command not find!")
        end

    end, {
        bang = true, 
        nargs = '+', 
        complete = function(
            lead, line, cursor
        )
            local fargs = vim.split(line, "%s+")
            -- Remove the first "Project" command call
            table.remove(fargs, 1)
            -- Remove last space or subcommand.
            -- Removing last space: 
            --   We need to complete args of last command so it is ok to remove last space.
            -- Removing last subcommand: 
            --   The last one is not white space. This means user not finish typing for this subcommand.
            --   We should complete this one, i.e. args of parent command.
            table.remove(fargs, #fargs)

            local node = M.commands

            while #fargs > 0 do
                local subcommand = fargs[1]
                local subcommands = node['subcommands']
                -- Dynamical subcommands
                if type(subcommands) == 'function' then
                    subcommands = subcommands(fargs)
                end

                if subcommands == nil or subcommands[subcommand] == nil then
                    break
                else
                    node = subcommands[subcommand]
                end

                table.remove(fargs, 1)
            end
            
            if node['complete'] ~= nil then
                return node.complete(line, fargs)
            elseif node['subcommands'] ~= nil then
                local subcommands = node['subcommands']
                -- Dynamical subcommands
                if type(subcommands) == 'function' then
                    subcommands = subcommands(fargs)
                end

                return vim.tbl_keys(
                    subcommands
                )
            else
                return {}
            end
        end
    }
)

return M

