-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")

-- Widget and layout library
local wibox = require("wibox")

-- Theme handling library
local beautiful = require("beautiful")

-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Request error handling.
local errors = require("errors")

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")

-- Custom themes.
local themes_path = os.getenv("HOME").."/.config/awesome/themes"
local theme_name = "sidonia"
local theme_path = string.format(
    "%s/%s/theme.lua", themes_path, theme_name
)
beautiful.init(theme_path)

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- This is used later as the default terminal and editor to run.
local apps = require("applications")

-- Custom modules.
local main_menu = require("main_menu")
local layout = require("layout")
local multimedia = require("multimedia")
local clients = require("clients")
local tags = require("tags")
local screens = require("screens")
local screenshot = require("screenshot")
local wm = require("wm")

-- Menubar configuration
menubar.utils.terminal = apps.default.terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Wibar
-- Create a textclock widget
local_clock = wibox.widget.textclock()
pst_clock = wibox.widget.textclock()
pst_clock.timezone = 'US/Pacific'
pst_clock.format = '/ %b %d, %H:%M PST'

local calendar = require(
    "awesome-wm-widgets.calendar-widget.calendar"
) ({
    placement = 'top_right',
    theme = 'nord',
    start_sunday = true
})

local_clock:connect_signal("button::press", function(_, _, _, button)
    if button == 1 then 
        calendar.toggle()
    end
end)

-- TODO: Use another timezone.
pst_clock:connect_signal("button::press", function(_, _, _, button)
    if button == 1 then 
        calendar.toggle()
    end
end)

-- Create a wibox for each screen and add it
local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, false)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ 
        "1", "2", "3", "4", "5", "6", "7", "8", "9"
    }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
        awful.button({ }, 1, function () awful.layout.inc( 1) end),
        awful.button({ }, 3, function () awful.layout.inc(-1) end),
        awful.button({ }, 4, function () awful.layout.inc( 1) end),
        awful.button({ }, 5, function () awful.layout.inc(-1) end)
    ))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = tags.taglist, 
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = clients.tasklist,
    }

    -- Create the wibox
    s.mywibox = awful.wibar({ 
        position = "top", 
        screen = s 
    })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            -- Main menu
            awful.widget.launcher({
                image = beautiful.distro_icon,
                menu = main_menu
            }),
        },
        
        s.mytasklist, -- Middle widget
        
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            --volume_widget,
            local_clock,
            pst_clock
        },
    }

    s.dock_wibox = awful.wibar({
        position = "bottom",
        screen = s
    })
    
    s.dock_wibox:setup {
        layout = wibox.layout.align.horizontal,
        {
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist
        },

        {
            {
                s.mypromptbox,
                left = 5,
                right = 5,
                widget = wibox.container.margin
            },
            layout = wibox.layout.fixed.horizontal
        },

        {   
            layout = wibox.layout.fixed.horizontal,
            multimedia.widgets.mpris
        }
    }

end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
    awful.button({}, 1, function () main_menu:hide() end),
    awful.button({}, 3, function () main_menu:toggle() end)
    -- awful.button({}, 4, awful.tag.viewnext),
    -- awful.button({}, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
root.keys(gears.table.join(
    wm.hotkeys,
    layout.hotkeys,
    multimedia.hotkeys,
    screenshot.hotkeys,
    apps.hotkeys,
    tags.hotkeys,
    clients.hotkeys.global,
    screens.hotkeys.global
))

clientkeys = gears.table.join(
    clients.hotkeys.client,
    screens.hotkeys.client
)

-- }}}

-- {{{ Client menu bar.
clientbuttons = gears.table.join(
    clients.buttons
)

-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = clients.rules(
    beautiful, clientkeys, clientbuttons
)
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

client.connect_signal("focus", function(c) 
    c.border_color = beautiful.border_focus 
end)
client.connect_signal("unfocus", function(c) 
    c.border_color = beautiful.border_normal 
end)
-- }}}

-- Deal with memory leakage.
gears.timer.start_new(600, function()
    return collectgarbage("step", 1024)
end)

