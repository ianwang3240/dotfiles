notify = require(
    'Project.build_system.notify'
)

local setup = function(M, opts)
    local builder = M.builder
    
    -- Read config
    builder.build_systems = opts['build_systems'] or {}
    
    builder.set_build_system = function(id, target)
        local id = id or 'make'
        local build_system = builder.build_systems[
            id
        ]

        -- Check if build system is configured.
        if build_system == nil then
            notify.error(
                'Build system not support: %s', id
            )
        end

        builder.build_system_id = id 
        builder.configs = build_system.configs or {}
        builder.target = target or build_system.target

        builder.errorformat = build_system.errorformat or {
            -- Include at least one pattern to avoid error.
            -- Ignore blank line.
            [[%-G\s%#]]
        }

        notify.info(
            'Build system is set to %s', id
        )
        notify.info(
            'Build system is set to %s', builder.target or 'none'
        )
    end

    builder.set_build_target = function(target)
        builder.target = target

        if target ~= nil then
            vim.notify(
                'Build target is set to '..target,
                vim.log.levels.INFO, {
                    title = 'Project Build System'
                }
            )
        else
            vim.notify(
                'Build target is set to none',
                vim.log.levels.INFO, {
                    title = 'Project Build System'
                }
            )
        end
    end

    M.register_command('builder', {
        subcommands = {
            set = {
                subcommands = {
                    build_system = {
                        execute = function(fargs)
                            require('Project').builder.set_build_system(
                                unpack(fargs)
                            )
                        end,
                        complete = function(fargs)
                            return vim.tbl_keys(
                                require('Project').builder.build_systems
                            )
                        end
                    },
                    target = {
                        execute = function(fargs)
                            require('Project').builder.set_build_target(
                                table.concat(fargs, ' ')
                            )
                        end
                    }
                }
            }
        }
    })
end

return {
    setup = setup
}
