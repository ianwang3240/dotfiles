" vim lsp
let g:lsp_diagnostics_signs_error = {'text': '>'}
let g:lsp_diagnostics_signs_warning = {'text': '>'}
let g:lsp_diagnostics_signs_hint = {'text': '>'}
let g:lsp_diagnostics_signs_information = {'text': '>'}

let g:lsp_document_code_action_signs_hint = {'text': '>'}

