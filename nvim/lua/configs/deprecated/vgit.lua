require('vgit').setup({
    settings = {
        live_blame = {
            -- Disable first, enable when needed.
            enabled = false
        },
        live_gutter = {
            enabled = true, 
            -- This allows users to navigate within a hunk
            edge_navigation = true,
        },
        authorship_code_lens = { 
            enabled = false 
        },
        hls = {
            GitSignsAdd = {
                fg = '#00ff00'
            },
            GitSignsChange = {
                fg = 'None'
            },
            GitSignsDelete = {
                fg = '#ff0000'
            },
        },
        signs = {
            definitions = {
                GitSignsAdd = {
                    texthl = 'GitSignsAdd',
                    text = '+',
                },
                GitSignsDelete = {
                    texthl = 'GitSignsDelete',
                    text = '-',
                },
                GitSignsChange = {
                    texthl = 'GitSignsChange',
                    text = '~',
                },
            }
        }
    }
})

vim.api.nvim_create_user_command('Gitblame', function()
    require('vgit').toggle_live_blame()
end, {bang=true})

vim.api.nvim_create_user_command('Githistory', function()
    require('vgit').buffer_history_preview()
end, {bang=true})
