local awful = require("awful")
local gears = require("gears")
local arrows = require("arrows")

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.tile.bottom,
    -- awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.floating,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

local hotkeys = gears.table.join(
    -- Layout selection.
    awful.key(
        { modkey, }, "\\", function () 
            awful.layout.inc(1)
        end, {
            description = "select next", 
            group = "layout"
        }
    ),

    awful.key(
        { modkey, "Shift" }, "\\", function () 
            awful.layout.inc(-1)
        end, {
            description = "select previous", 
            group = "layout"
        }
    ),

    awful.key(
        { modkey, }, 'u', function()
            awful.layout.set(
                awful.layout.suit.tile
            )
        end, {
            description = "switch to tile layout",
            group = "layout"
        }
    ),

    awful.key(
        { modkey, }, 'o', function()
            awful.layout.set(
                awful.layout.suit.fair.horizontal
            )
        end, {
            description = "switch to horizontal fair layout",
            group = "layout"
        }
    ),

    awful.key(
        { modkey, }, 'i', function()
            awful.layout.set(
                awful.layout.suit.fair
            )
        end, {
            description = "switch to vertical fair layout",
            group = "layout"
        }
    ),

    awful.key(
        { modkey, }, '.', function () 
            awful.tag.incmwfact( 0.05)
        end, {
            description = "increase master width factor", 
            group = "layout"
        }
    ),

    awful.key(
        { modkey, }, ',', function () 
            awful.tag.incmwfact(-0.05)
        end, {
            description = "decrease master width factor", 
            group = "layout"
        }
    ),
    
    awful.key(
        { modkey , "Shift" }, ']', function() 
            awful.tag.incnmaster( 1, nil, true) 
        end, {
            description = "increase the number of master clients", 
            group = "layout"
        }
    ),

    awful.key(
        { modkey, "Shift" }, "[", function() 
            awful.tag.incnmaster(-1, nil, true) 
        end, {
            description = "decrease the number of master clients", 
            group = "layout"
        }
    ),
    
    awful.key(
        { modkey, }, "]", function () 
            awful.tag.incncol( 1, nil, true)    
        end, {
            description = "increase the number of columns", 
            group = "layout"
        }
    ),

    awful.key(
        { modkey, }, "[", function () 
            awful.tag.incncol(-1, nil, true)    
        end, {
            description = "decrease the number of columns", 
            group = "layout"
        }
    ),
    
    -- Trigger floating.
    awful.key(
        { modkey, }, 'space', function() 
        local c = client.focus
            if c then
                c.floating = not c.floating
                if c.floating then
                    -- Give a proper size and center to screen.
                    c:geometry({
                        width = c.screen.workarea.width * 0.9,
                        height = c.screen.workarea.height * 0.9
                    })
                    awful.placement.centered(c)
                    c.raise = true
                end
            end
        end, {
            description = "toggle floating", 
            group = "layout"
        }
    )
)

return {
    hotkeys = hotkeys
}

