vim.api.nvim_set_hl(0, 'GitGutterAdd', {
    fg = '#00FF00'
})
vim.api.nvim_set_hl(0, 'GitGutterChange', {
    fg = '#00AAFF'
})
vim.api.nvim_set_hl(0, 'GitGutterDelete', {
    fg = 'red'
})

