local awful = require("awful")
local gears = require("gears")
local beautiful = require("beautiful")

local apps = require("applications")

local hotkeys_popup = require("awful.hotkeys_popup")

local awesome_menu = {
    { 
        "hotkeys", function() 
            hotkeys_popup.show_help(
                nil, awful.screen.focused()
            ) 
        end
    },

    { 
        "manual", 
        apps.default.cli("man awesome")
    },

    { 
        "edit config", 
        apps.default.edit(awesome.conffile)
    },

    { 
        "restart", 
        awesome.restart 
    },
   
    { 
        "quit", function()
            awesome.quit() 
        end 
    },
}

-- Borrow icons from awesome-wm-widgets.
local system_icons = string.format(
    "%s/.config/awesome/awesome-wm-widgets/logout-menu-widget/icons/", 
    os.getenv("HOME")
)
local system_menu = {
    { "Log out", function()
        awesome.quit()
    end, system_icons.."log-out.svg" },

    { "Lock", function()
        -- TODO: Missing locker!
        awful.spawn("")
    end, system_icons.."lock.svg" },

    { "Reboot", function()
        awful.spawn("loginctl reboot")
    end, system_icons.."refresh-cw.svg" },

    { "Suspend", function()
        awful.spawn("loginctl suspend")
    end, system_icons.."moon.svg" },

    { "Power off",  function()
        awful.spawn("loginctl shutdown")
    end, system_icons.."power.svg" }
}

local main_menu = awful.menu({items = { 
    { "awesome", awesome_menu, beautiful.awesome_icon },
    { "system", system_menu, system_icons.."power.svg" }
    -- { "open terminal", terminal }
}})

return main_menu
