require('Project').setup {}

vim.keymap.set({'n', 'i'}, '<F4>', function()
    require('Project').builder.build_and_run('release')
end, {
    silent = true, noremap = true
})
vim.keymap.set({'n', 'i'}, '<F28>', function()
    require('Project').builder.build('release')
end, {
    silent = true, noremap = true
})

vim.keymap.set({'n', 'i'}, '<F5>', function()
    require('Project').builder.build_and_run('debug')
end, {
    silent = true, noremap = true
})
vim.keymap.set({'n', 'i'}, '<F29>', function()
    require('Project').builder.run('debug')
end, {
    silent = true, noremap = true
})
