-- Set colorscheme
vim.cmd.colorscheme("vim")

-- Terminal color support.
vim.opt.termguicolors = true

-- Auto save and hover update.
-- Currently handled by antoinemadec/FixCursorHold.nvim plugin.
-- vim.opt.updatetime = 1000

-- Syntex and indention.
vim.cmd("syntax on")
-- vim.opt.autoindent = true
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

-- Line numbers.
vim.opt.number = true
vim.opt.relativenumber = true

-- Backspace support.
vim.opt.backspace = {'indent', 'eol', 'start'}

-- Selections.
vim.opt.virtualedit='block'
-- Selection color.
vim.api.nvim_set_hl(0, "Visual", {
    bg = '#3F3F3F'
})

-- Leader key
vim.g.mapleader=','

-- Leader key timeout.
vim.opt.timeout = false
vim.opt.ttimeout = false

-- Colors.
vim.api.nvim_set_hl(0, 'VertSplit', {
    bg = 'None', fg = 'None'
})
vim.api.nvim_set_hl(0, 'Pmenu', {
    bg = 'None', fg = 'None'
})

-- Sign column.
vim.opt.signcolumn = 'yes'
vim.api.nvim_set_hl(0, 'SignColumn', {
    bg = 'None'
})

-- Folding
vim.api.nvim_set_hl(0, 'Folded', {
    bg = 'None'
})

-- Security
vim.opt.modeline = false

-- Don't let Vim's "Found a swap file" message block input
-- vim.opt.shortmess = 'A'

vim.opt.mouse = ''

-- Spawn terminal in same directory
vim.api.nvim_create_user_command(
    'Terminal', function()
        vim.api.nvim_command(string.format(
            -- We remove VIMRUNTIME variable.
            -- Some shell prompt will think they are in vim terminal and refuse to work.
            'silent !VIMRUNTIME="" alacritty --working-directory "%s" &> /dev/null &', vim.fn.getcwd()
        ))
    end, {bang=true}
)

-- Matchparen cause some trobule, replaced by a matchparen.nvim
vim.g.loaded_matchparen = 1

