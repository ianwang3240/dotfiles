-- Setup plugin manager.
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

-- Plugins
require("lazy").setup({
    {
        "vhyrro/luarocks.nvim",
        priority = 1000, -- We'd like this plugin to load first out of the rest
        config = true, -- This automatically runs `require("luarocks-nvim").setup()`
    },
    {
        -- Dashboard
        'goolord/alpha-nvim',
        dependencies = { 
            'nvim-tree/nvim-web-devicons' 
        },
        event = "VimEnter",
        config = function ()
            require("configs.alpha")
        end
    }, {
		-- Custom UI
		"MunifTanjim/nui.nvim", 
		config = function()
			require("configs.nui")
		end
	}, {
		-- Status bar
		"nvim-lualine/lualine.nvim", 
        config = function()
            require("configs.lualine")
        end
	}, {
        -- Jump indicator.
        "danilamihailov/beacon.nvim", 
        config = function()
            require("configs.beacon")
        end
    }, {
        -- Surround contents with quote, bracket, etc.
        "kylechui/nvim-surround", 
        config = function()
            require("configs.surround")
        end
    }, {
        -- File manager.
        "nvim-neo-tree/neo-tree.nvim", 
        branch="v3.x",
        dependencies = {
            "MunifTanjim/nui.nvim",
            "nvim-tree/nvim-web-devicons"
        },
        config = function()
            require("configs.neo-tree")
        end
    }, {
        -- File fuzzy search.
        "nvim-telescope/telescope.nvim", 
        dependencies = {
            "nvim-lua/plenary.nvim"
        },
        config = function()
            require("configs.telescope")
        end
    }, {
        -- Snippet engine
        "L3MON4D3/LuaSnip", 
        config = function()
            require("configs.luasnip")
        end
    }, {
        -- LSP service.
        "neovim/nvim-lspconfig", 
        dependencies = {
            "hrsh7th/nvim-cmp",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-path",
            "L3MON4D3/LuaSnip",
            "saadparwaiz1/cmp_luasnip",
            "ray-x/lsp_signature.nvim",
            "ray-x/cmp-treesitter",
            -- UI
            "onsails/lspkind-nvim",
            "MunifTanjim/nui.nvim",
            -- "j-hui/fidget.nvim"
            "mrded/nvim-lsp-notify"
        },
        config = function()
            require("configs.lsp")
        end
    }, 

    -- {
    --     -- LSP progress.
    --     "j-hui/fidget.nvim",
    --     branch = "legacy",
    --     config = function()
    --         require("configs.fidget")
    --     end
    -- }, 
    
    {
        "mrded/nvim-lsp-notify",
        dependencies = {
            "rcarriga/nvim-notify"
        },
        config = function()
            require("configs.lsp.lsp-notify")
        end,
    }, {
        -- Syntex highlighting.
        "nvim-treesitter/nvim-treesitter",
        build=":TSUpdate",
        config = function()
            require("configs.treesitter")
        end
    }, 

    -- {
    --     "yioneko/nvim-yati", 
    --     dependencies = {
    --         "nvim-treesitter/nvim-treesitter" 
    --     }
    -- }, 

    {
        -- Better bracket matching.
        "monkoose/matchparen.nvim",
        config = function()
            require("configs.matchparen")
        end
    },

    -- Seems conflict with treesitter highlighting.
    -- {
    --      "norcalli/nvim-colorizer.lua",
    --      config = function()
    --        	require("configs.colorizer")
    --      end
    -- },

    -- {
    --     "tanvirtin/vgit.nvim", 
    --     dependencies = {
    --         "nvim-lua/plenary.nvim"
    --     },
    --     config = function()
    --         require("configs.vgit")
    --     end
    -- },

    {
        "lewis6991/gitsigns.nvim",
        config = function()
            require("configs.gitsigns")
        end
    },

    -- {
    --      "f-person/git-blame.nvim",
    --      config = function()
    -- 	        require("configs.git-blame")
    --      end
    -- },

    -- {
    --      "sindrets/diffview.nvim",
    --      config = function()
    --          require("configs.diffview")
    --      end
    -- },

    {
        -- Terminal
        "numToStr/FTerm.nvim",
        config = function()
            require("configs.fterm")
        end
    }, {
        -- Quickfix window.
        "kevinhwang91/nvim-bqf", 
        config = function()
            require("configs.bqf")
        end
    }, {
        -- Hex viewer.
        "RaafatTurki/hex.nvim",
        config = function()
            require("configs.hex")
        end
    },

    -- Trouble.nvim currently buggy with :make
    -- {
    --      "folke/trouble.nvim",
    --      config = function()
    --          require("configs.trouble")
    --      end
    -- },

    {
        -- TODO highlight
        "folke/todo-comments.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim"
        },
        config = function()
            require("configs.todo")
        end
    }, {
        -- Code outline
        "stevearc/aerial.nvim", 
        config = function()
            require("configs.aerial")
        end
    }, {
        "michaelb/sniprun",
        dependencies = {
            "rcarriga/nvim-notify"
        }, 
        build = "bash ./install.sh",
        config = function()
            require("configs.sniprun")
        end
    }, {
        "rcarriga/nvim-notify",
        config = function()
            require("configs.notify")
        end
    },

    -- {
    --      "HiPhish/desktop-notify-nvim",
    --      config=function()
    --          require("configs.notify")
    --      end
    -- },

    {
        "nvim-neorg/neorg",
        dependencies = {
            "vhyrro/luarocks.nvim",
            "nvim-lua/plenary.nvim", 
            "nvim-neorg/neorg-telescope"
        }, 
        config = function()
            require("configs.neorg")
        end
    }, {
        -- Input method
        "pi314/ime.vim",
        dependencies = {
            "pi314/ime-phonetic.vim",
            "pi314/ime-wide.vim",
            "pi314/ime-runes.vim"
        },
        -- Since ime.vim read vim.g for configuration.
        -- it should be set before plugin load.
        init = function()
            require("configs.ime")
        end
    }, {
        -- Prevent open file in side panel
        "stevearc/stickybuf.nvim", 
        config = function()
            require("configs.stickybuf")
        end
    }, {
        -- Cursorhold update fix.
        "antoinemadec/FixCursorHold.nvim", 
        config=function()
            vim.g.cursorhold_updatetime = 500
        end
    }
})

