local awful = require("awful")
local gears = require("gears")

local mpris_widget = require(
    "awesome-wm-widgets.mpris-widget"
) ()

-- Volume
-- Disable due to memory leak.
--[[local VolumeWidget = require(
    "awesome-wm-widgets.volume-widget.volume"
) 

local volume_widget = VolumeWidget({
    widget_type = "arc"
})--]]

local volume_hotkeys = gears.table.join(
    -- Volume key need to bind to volume widget.
    
    -- Use multimedia keys.
    awful.key(
        {}, "XF86AudioRaiseVolume",
        function()
            --VolumeWidget:inc(5)
            awful.spawn("amixer -q -M sset Master 5%+")
        end,
        {description = "raise volume", group = "sound"}
    ),

    awful.key(
        {}, "XF86AudioLowerVolume",
        function()
            --VolumeWidget:dec(5)
            awful.spawn("amixer -q -M sset Master 5%-")
        end,
        {description = "lower volume", group = "sound"}
    ),

     awful.key(
        {}, "XF86AudioMute",
        function()
            --VolumeWidget:toggle()
            awful.spawn("amixer -q sset Master toggle")
        end,
        {description = "(un)mute", group = "sound"}
    ),
    
    -- Use regular key.
    awful.key(
        {modkey, }, "=",
        function()
            awful.spawn("amixer -q -M sset Master 5%+")
        end,
        {description = "raise volume", group = "sound"}
    ),

    awful.key(
        {modkey, }, "-",
        function()
            awful.spawn("amixer -q -M sset Master 5%-")
        end,
        {description = "lower volume", group = "sound"}
    ),

    awful.key(
        {modkey, }, "0",
        function()
            awful.spawn("amixer -q -M sset Master toggle")
        end,
        {description = "(un)mute", group = "sound"}
    )
)

local player_hotkeys = gears.table.join(
    awful.key(
        {}, "XF86AudioPlay",
        function()
            awful.spawn("playerctl play-pause")
        end,
        {description = "play/pause", group = "multi-media"}
    ),
    
    awful.key(
        {modkey, }, "\\",
        function()
            awful.spawn("playerctl play-pause")
        end,
        {description = "play/pause", group = "multi-media"}
    ),

    awful.key(
        {}, "XF86AudioStop",
        function()
            awful.spawn("playerctl stop")
        end,
        {description = "stop", group = "multi-media"}
    ),

    awful.key(
        {}, "XF86AudioNext",
        function()
            awful.spawn("playerctl next")
        end,
        {description = "next track", group = "multi-media"}
    ),

    awful.key(
        {}, "XF86AudioPrev",
        function()
            awful.spawn("playerctl previous")
        end,
        {description = "previous track", group = "multi-media"}
    ),

    awful.key(
        {}, "XF86AudioForward",
        function()
            awful.spawn("playerctl position 5+")
        end,
        {description = "forward", group = "multi-media"}
    ),

    awful.key(
        {'Shift'}, "XF86AudioForward",
        function()
            awful.spawn("playerctl next")
        end,
        {description = "next track", group = "multi-media"}
    ),

    awful.key(
        {}, "XF86AudioRewind",
        function()
            awful.spawn("playerctl position 5+")
        end,
        {description = "rewind", group = "multi-media"}
    ),

    awful.key(
        {'Shift'}, "XF86AudioRewind",
        function()
            awful.spawn("playerctl previous")
        end,
        {description = "previous track", group = "multi-media"}
    )
)

return {
    widgets = {
        mpris = mpris_widget
    },
    hotkeys = gears.table.join(
        volume_hotkeys,
        player_hotkeys
    )
}
