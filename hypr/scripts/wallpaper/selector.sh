#!/bin/bash

dmenu="fuzzel -d"
wallpapers=~/Pictures/wallpapers

# Get all display from compositor (hyprland in this case)
display=$(hyprctl monitors -j | jq -r ".[].name" | $dmenu -p "Display> ")

if [[ -z $display ]]; then
    exit
fi

wallpaper=$(find -L $wallpapers -type f | grep -E "\.(jpg|jpeg|png|gif)$" | $dmenu -p "Wallpaper> ")

if [[ ! -z $wallpaper ]]; then
    swww img -o "$display" "$wallpaper" 
fi

