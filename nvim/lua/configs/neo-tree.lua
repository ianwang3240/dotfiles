require('nvim-web-devicons').setup {
    override = {
        norg = {
            icon = "ﴬ",
            name = "Neorg"
        }
    }
}

-- Some special files should be prioritize.
local special_files = {
    -- Rust
    'mod.rs', 'main.rs', 'lib.rs',
    -- C/C++
    'main.c', 'main.cpp', 'main.h', 'main.hpp', 'CMakeLists.txt',
    -- Python
    '__init__.py', 'setup.py',
    -- Lua
    'init.lua',
    -- Others
    'Makefile'
}

-- A sorting function which consider special files.
local function priority_sorting(a, b)
    if a.type == b.type then
        prioritize_a = vim.tbl_contains(special_files, a.name)
        prioritize_b = vim.tbl_contains(special_files, b.name)

        if prioritize_a and not prioritize_b then
            return true
        end

        if prioritize_b and not prioritize_a then
            return false
        end

        return a.path < b.path
    else
        return a.type < b.type
    end
end

-- Trash item using trash on mac and gio trash on linux system.
local function trash(path)
    if vim.fn.has('macunix') == 1 then
        vim.fn.system(string.format(
            'trash "%s"', vim.fn.fnameescape(path)
        ))
    else
        vim.fn.system(string.format(
            'gio trash "%s"', vim.fn.fnameescape(path))
        )
    end
end

vim.g.netrw_keepdir = 0

require('neo-tree').setup {
    sources = {
        "filesystem", 
        "buffers", 
        "document_symbols"
    },

    default_source = "filesystem",

    sort_function = priority_sorting,

    enable_git_status = true,
    git_status_async = true,

    enable_diagnostics = true,

    use_popups_for_input = false,
    popup_border_style = "rounded",

    default_component_configs = {
        indent_marker = "│",
        last_indent_marker = "└",
        icon = {
            folder_closed = "",
            folder_open = "",
            folder_empty = "",
            default = '',
        },
        modified = {
            symbol = "",
            highlight = "NeoTreeModified",
        },
        git_status = {
            symbols = {
                -- Change type
                added     = "+",
                deleted   = "-",
                modified  = "~",
                renamed   = "➜",
                -- Status type
                untracked = "★",
                ignored   = "◌",
                unstaged  = "",
                staged    = "",
                conflict  = "",
            },
            align = "right",
        },
    },

    window = {
        close_if_last_window = false,
        position = "left",
        width = 30,
    },

    event_handlers = {
        -- {
        --     event = "file_opened",
        --     handler = function(args)
        --         require('neo-tree.command').execute({
        --             action='close',
        --             source='filesystem'
        --         })
        --     end
        -- },
        
        {
            -- Clear filter whenever open the tree.
            event = "neo_tree_window_after_open",
            handler = function(args)
                if args.source == 'filesystem' then
                    local state = require(
                        "neo-tree.sources.manager"
                    ).get_state(args.source)
                    
                    -- Clear filter without refresh (the second argument), which breaks netrw hijack.
                    require(
                        "neo-tree.sources.filesystem"
                    ).reset_search(state, false)
                end
            end
        }
    },

    filesystem = {
        filtered_items = {
            hide_dotfiles = false,
            hide_gitignored = false,
            hide_by_name = {
                ".DS_Store",
                "thumbs.db"
            },
        },

        follow_current_file = {
            enable = false
        },

        hijack_netrw_behavior = "open_default",

        cwd_target = {
            current = "window",
            sidebar = "global"
        },

        -- Cause problem when open too many file.
        use_libuv_file_watcher = false,
        
        window = {
            mappings = {
                ["O"] = "system_open",
                ["/"] = "clear_and_filter_as_you_type",
                ["l"] = "open_and_close_window",
                ["L"] = "open",
                ["h"] = "close_node",
                ["."] = "set_root_or_parent",
                ["t"] = "open_tabnew",
                ["T"] = "open_terminal",
                ["<Esc>"] = "preview_cancel_or_close_window",
                ["<Return>"] = "open_and_close_window"
                
                -- Disable some keys.
                -- ["o"] = "noop"
                -- ["oc"] = "noop",
                -- ["od"] = "noop",
                -- ["og"] = "noop",
                -- ["om"] = "noop",
                -- ["on"] = "noop",
                -- ["os"] = "noop",
                -- ["ot"] = "noop",
            },
        },

        commands = {
            open_and_close_window = function(state)
                local node = state.tree:get_node()

                if node.type == 'directory' then
                    state.commands['toggle_node'](state)
                else
                    state.commands['open'](state)
                    state.commands['close_window'](state)
                end
            end,

            system_open = function(state)
                local node = state.tree:get_node()
                local path = node:get_id()

                if vim.fn.has('macunix') == 1 then
                    vim.fn.system(string.format(
                        'open "%s" &!', vim.fn.fnameescape(path)
                    ))
                else
                    vim.fn.system(string.format(
                        'xdg-open "%s" &!', vim.fn.fnameescape(path))
                    )
                end
            end,

            clear_and_filter_as_you_type = function(state)
                state.commands["clear_filter"](state)
                state.commands["filter_as_you_type"](state)
            end,
            
            set_root_or_parent = function(state)
                local node = state.tree:get_node()

                if node.type ~= 'directory' then
                    require('neo-tree.ui.renderer').focus_node(
                        state, node:get_parent_id()
                    )
                end

                state.commands["set_root"](state)
            end,

            open_terminal = function(state)
                local node = state.tree:get_node()

                if node.type ~= 'directory' then
                    path = node:get_parent_id()
                else
                    path = node:get_id()
                end

                vim.fn.system(string.format(
                    -- We remove VIMRUNTIME variable.
                    -- Some shell prompt will think they are in vim terminal and refuse to work.
                    'VIMRUNTIME="" alacritty --working-directory "%s" &> /dev/null &!', vim.fn.fnameescape(path)
                ))
            end,
            
            -- Close preview or close neotree window.
            preview_cancel_or_close_window = function(state)
                local preview = require("neo-tree.sources.common.preview")

                if preview.is_active() then
                    state.commands["cancel"](state)
                else
                    state.commands["close_window"](state)
                end
            end,

            -- Use trash instead delete.
            delete = function(state)
                local inputs = require("neo-tree.ui.inputs")
                local path = state.tree:get_node().path

                inputs.confirm(
                    string.format("Trash %s?", path), 
                    function(confirmed)
                        if confirmed then 
                            trash(path)
                            require("neo-tree.sources.manager").refresh(
                                state.name
                            )
                        end
                    end
                )
            end,

            -- Use trash instead delete for visual mode.
            delete_visual = function(state, selected_nodes)
                -- Input prompt
                local inputs = require("neo-tree.ui.inputs")
                local count = #selected_nodes

                inputs.confirm(
                    string.format("Trash %d item(s)?", #selected_nodes), 
                    function(confirmed)
                        if confirmed then 
                            for _, node in ipairs(selected_nodes) do
                                trash(node.path)
                            end

                            require("neo-tree.sources.manager").refresh(
                                state.name
                            )
                        end
                    end
                )
            end,
        },
    },

    document_symbols = {
        window = {
            mappings = {
                ["l"] = "expand_or_jump_and_close",
                ["L"] = "expand_or_jump",
                ["h"] = "close_node",
                ["<Esc>"] = "preview_cancel_or_close_window",
                ["<Return>"] = "jump_and_close"
            },
        },

        commands = {
            -- Jump to symbol and close tree.
            jump_and_close = function(state)
                state.commands["jump_to_symbol"](state)
                state.commands["close_window"](state)
            end,

            -- If node is expandable, expand it first.
            -- If not, we assume user want to jump to symbol.
            expand_or_jump_and_close = function(state)
                local node = state.tree:get_node()
                if node:is_expanded() then
                    state.commands["jump_to_symbol"](state)
                    state.commands["close_window"](state)
                else
                    if node:has_children() then
                        state.commands["toggle_node"](state)
                    else
                        state.commands["jump_to_symbol"](state)
                        state.commands["close_window"](state)
                    end
                end
            end,
            
            -- If node is expandable, expand it first.
            -- If not, we assume user want to first jump to symbol
            -- and then close the tree to keep UI clean.
            expand_or_jump = function(state)
                local node = state.tree:get_node()
                if node:is_expanded() then
                    state.commands["jump_to_symbol"](state)
                else
                    if node:has_children() then
                        state.commands["toggle_node"](state)
                    else
                        state.commands["jump_to_symbol"](state)
                    end
                end
            end,
            
            -- Close preview or close neotree window.
            preview_cancel_or_close_window = function(state)
                local preview = require("neo-tree.sources.common.preview")

                if preview.is_active() then
                    state.commands["cancel"](state)
                else
                    state.commands["close_window"](state)
                end
            end
        }
    }
}

local function tree_exist(state)
    return require('neo-tree.ui.renderer').window_exists(
        state
    )
end

local function focus_or_toggle_tree(source, reveal)
    vim.cmd("stopinsert")

    local command = require('neo-tree.command')
    local manager = require('neo-tree.sources.manager')

    local state = manager.get_state(source)
    if tree_exist(state) then
        if vim.api.nvim_get_current_win() == state.winid then
            command.execute({
                action = 'focus',
                source = source,
                toggle = true,
                reveal = reveal
            })
        else
            command.execute({
                action = 'focus',
                source = source,
                reveal = reveal
            })
        end
    else
        command.execute({
            action = 'focus',
            source = source,
            reveal = reveal
        })
    end
end

local function focus_or_open_tree(source, reveal)
    vim.cmd("stopinsert")

    local command = require('neo-tree.command')
    local manager = require('neo-tree.sources.manager')

    local state = manager.get_state(source)
    if reveal then
        if tree_exist(state) then
            -- If we stay in a tree, we cannot reveal anything for current window
            -- since tree is also a window.
            -- To solve this problem, we jump to next window and see if we can reveal it.
            if vim.api.nvim_get_current_win() == state.winid then
                vim.cmd('wincmd w')
            end
        end
    end

    command.execute({
        action = 'focus',
        source = source,
        toggle = false,
        reveal = reveal
    })
end

vim.api.nvim_create_user_command(
    'Ls', 'Neotree buffers float', {bang = true}
)
vim.api.nvim_create_user_command(
    'LS', 'Neotree buffers float', {bang = true}
)
vim.api.nvim_create_user_command(
    'Symbols', 'Neotree document_symbols', {bang = true}
)

vim.keymap.set(
    {'i', 'n'}, '<F1>', function()
        focus_or_open_tree('filesystem', true)
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    {'i', 'n'}, '<F2>', function()
        focus_or_toggle_tree('filesystem', false)
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    {'i', 'n'}, '<F3>', function()
        focus_or_open_tree('document_symbols', true)
    end, {silent = true, noremap = true}
)

