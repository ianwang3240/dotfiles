return function()
    -- nvim-cmp setup
    local cmp = require('cmp')

    -- Snip engine.
    local luasnip = require('luasnip')

    -- Icom support.
    local lspkind = require('lspkind')

    -- Add additional capabilities supported by nvim-cmp
    capabilities = require('cmp_nvim_lsp').default_capabilities()

    cmp.setup {
        formatting = {
            format = lspkind.cmp_format({
                mode = 'symbol_text',
                maxwidth = 50,
                -- Further modification callback.
                -- Do nothing currently.
                before = function(entry, vim_item)
                    return vim_item
                end
            })
        },
        window = {
            completion = {
                border = 'rounded',
            },
            documentation = {
                border = 'rounded'
            }
        },
        snippet = {
            expand = function(args)
                require('luasnip').lsp_expand(args.body)
            end,
        },
        mapping = {
            ['<Tab>'] = function(fallback)
                if cmp.visible() then
                    cmp.select_next_item()
                elseif luasnip.expand_or_jumpable() then
                    luasnip.expand_or_jump()
                else
                    fallback()
                end
            end,
            ['<S-Tab>'] = function(fallback)
                if cmp.visible() then
                    cmp.select_prev_item()
                elseif luasnip.jumpable(-1) then
                    luasnip.jump(-1)
                else
                    fallback()
                end
            end,
            ['<CR>'] = cmp.mapping.confirm({
                select = false
            })
        },
        sources = {
            { name = 'nvim_lsp' },
            { name = 'luasnip' },
            { name = 'treesitter' },
            { name = 'path' },
            -- { name = "nvim_lsp_signature_help" },
        },
    }

    return capbilities
end
