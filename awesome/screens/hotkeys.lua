local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local naughty = require("naughty")
local beautiful = require("beautiful")

local global_hotkeys = gears.table.join(
    awful.key(
        { modkey, }, "Escape", 
        function() 
            awful.screen.focus_relative( 1) 
            screen.emit_signal(
                "property::focus", 
                awful.screen.focused()
            )
        end,
        {description = "focus the next screen", group = "screen"}
    ),

    awful.key(
        { modkey, "Shift"}, "Escape", 
        function() 
            awful.screen.focus_relative(-1)
            screen.emit_signal(
                "property::focus", 
                awful.screen.focused()
            )
        end,
        {description = "focus the previous screen", group = "screen"}
    )
)

local client_hotkeys = {}

-- Helper functions.

-- Get all clients from given tags.
function clients_of_tags(tags)
    local clients = {}
    for _, t in ipairs(tags) do
        for _, c in ipairs(t:clients()) do 
            table.insert(clients, c)
        end
    end

    return clients
end

-- Move clients to target screen and preserve corrsponding tag if possible.
function move_clients_to_screen(clients, target)
    for _, c in pairs(clients) do
        local tags = c:tags()
        local new_tags = corresponding_tags(tags, target)
        
        c.screen = target
        c:tags(new_tags)
    end
end

-- Get corresponding tags from target screen.
function corresponding_tags(tags, target_screen)
    new_tags = {}
    for idx, t in ipairs(tags) do
        new_tags[idx] = target_screen.tags[
            math.min(t.index, #(target_screen.tags))
        ]
    end

    return new_tags
end

function isin(item, list)
    for _, t in ipairs(list) do
        if item == t then
            return true
        end
    end
    return false
end

for i = 1, 4 do
    client_hotkeys = gears.table.join(
        client_hotkeys,
        
        awful.key(
            { modkey, "Shift" }, "F"..i,
            function(c)
                local target = screen[i]
                
                if target then
                    c:move_to_screen(target)
                end
            end, {
                description = "move focused client to screen #"..i, 
                group = "screen"
            }
        )
    )

    global_hotkeys = gears.table.join(
        global_hotkeys,

        awful.key(
            { modkey, }, "F"..i,
            function()
                local s = screen[i]
                if s then
                    awful.screen.focus(s)
                    screen.emit_signal(
                        "property::focus", 
                        awful.screen.focused()
                    )
                end
            end,
            {description = "focus on screen #"..i, group = "screen"}
        ),
        
        awful.key(
            { modkey, "Control"}, "F"..i,
            function()
                -- Swap all content for active tags between screen corresponding tags,
                -- i.e. send all client in current screen with active tags to taget screen and
                -- send all client in target screen with corresponding tags to current screen.
                local target = screen[i]
                local focused = awful.screen.focused()
                 
                if target and focused then
                    local focused_tags = focused.selected_tags
                    local target_tags = corresponding_tags(focused_tags, target)
                    
                    local target_screen_clients = clients_of_tags(
                        target_tags
                    )
                    local focused_screen_clients = clients_of_tags(
                        focused_tags
                    )

                    move_clients_to_screen(
                        target_screen_clients, focused
                    )
                    move_clients_to_screen(
                        focused_screen_clients, target
                    )
                    
                    -- Active corresponding tags.
                    for _, t in ipairs(target.tags) do
                        t.selected = isin(t, target_tags)
                    end

                    -- Change focus.
                    awful.screen.focus(target)
               end
            end,
            {description = "swap focused screen with #"..i, group = "screen"}
        )
    )

end

return {
    global = global_hotkeys,
    client = client_hotkeys
}

