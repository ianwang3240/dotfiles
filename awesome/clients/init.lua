local hotkeys = require("clients.hotkeys")
local buttons = require("clients.buttons")
local tasklist = require("clients.tasklist")
local rules = require("clients.rules")

require("clients.signals")

return {
    hotkeys = hotkeys,
    tasklist = tasklist,
    buttons = buttons,
    rules = rules
}
