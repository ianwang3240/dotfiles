#!/bin/bash

script=$HOME/.config/hypr/scripts/screenshot/display.sh

# Get all display from compositor (hyprland in this case)
displays=$(hyprctl monitors -j | jq -r ".[].name")

# Get timestamp, which will be used as part of output name
timestamp=$(date "+%Y%m%d_%Hh%Mm%Ss%N")

# Capture each display one by one.
for display in $displays; do
    # Include timestamp and display name in output file name
    filename=screenshot_${timestamp}_${display}.png
    
    # Do capturing
    $script $display $filename &
done

