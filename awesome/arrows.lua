local awful = require('awful')
local gears = require('gears')

local M = {}

M.key = function(mod, _key, ...)
    if _key == 'Up' then
        return gears.table.join(
            awful.key(mod, 'Up', ...),
            awful.key(mod, 'w', ...),
            awful.key(mod, 'k', ...)
        )
    elseif _key == 'Down' then
        return gears.table.join(
            awful.key(mod, 'Down', ...),
            awful.key(mod, 's', ...),
            awful.key(mod, 'j', ...)
        )
    elseif _key == 'Left' then
        return gears.table.join(
            awful.key(mod, 'Left', ...),
            awful.key(mod, 'a', ...),
            awful.key(mod, 'h', ...)
        )
    elseif _key == 'Right' then
        return gears.table.join(
            awful.key(mod, 'Right', ...),
            awful.key(mod, 'd', ...),
            awful.key(mod, 'l', ...)
        )
    end
end

return M

