# Dotfiles

This is a collection of my configuration files on Linux with awesome window manager (X11) and hyprland compositor (wayland).
This repo is not design for installation but for learning purpose and future reference.
Many components are depend on existing projects so don't forget to give credits.

## Install

You can copy any code and modify it to meet your needs.
Instead of copy, you can also create symbolic links to config locations.
The following sections are minimal setup for each component.
Note that the procedures are base on ArtixLinux and may not exactly same for your distro.

### Color Scheme

My color scheme is design on dark theme base on anime series *Knight of Sidonia*.
The final version is dimmer then actually is in animation but more comfortable for eyes.
You may want to use lxapperance to setup color theme if you don't have any other desktop environment with configuration tools installed.

### Common

There are two major window system currently, namely X11 and wayland.
Some application are listed here and mark as common.
This means that it works well in two different environment, including using xwayland.

<details>
<summary>Configurations</summary>

#### [Alacritty](https://github.com/alacritty/alacritty)

Alacritty is a fast, cross-platform, OpenGL terminal emulator.
Install alacritty, copy or link `alacritty/alacritty.yaml` in this repo to `~/.config/alacritty/alacritty.yaml` 
and hit `Super+X` (if you use default keybinding) then a terminal will show up in font of you.
The color theme is configured to use light blue to meet global color theme. 
You can change it to other fancy color in `alacritty.yaml`.

#### [neovim](https://github.com/neovim/neovim)

Neovim is a new vim fork which I currently moving to.
This configuration are written with lua and use [packer.nvim](https://github.com/wbthomason/packer.nvim) as plugin manager.
Configured plugins including LSP services ([lspconfig](https://github.com/neovim/nvim-lspconfig),
[nvim-cmp](https://github.com/hrsh7th/nvim-cmp), etc.),
file managements ([telescope](https://github.com/nvim-telescope/telescope.nvim),
[neo-tree](https://github.com/nvim-neo-tree/neo-tree.nvim), etc.), 
git insepctions ([vgit.nvim](https://github.com/tanvirtin/vgit.nvim), etc.),
[Neorg](https://github.com/nvim-neorg/neorg), and some other miscellaneous plugins.
The principle to select plugin is prefer which natively support nvim and written in lua over vim one.
Each plugin configuration is placed under `lua/configs`.
You can disable any of them in `lua/plugins.lua`.

To install configuration, first install neovim and packer.nvim.
Note that I use manual installed packer instead of AUR version so that packer can handle update by itself.
Additional configuration may needed to use AUR version of packer.
After install neovim and packer.nvim, copy or link nvim directory from this repo to `~/.config/nvim` and run `:PackerSync` in nvim.
This should setup plugins properly.

#### Vim(Deprecated)

The configurations only contain some vim plugins and basic setup those I familiar with.
It use [vim-plug](https://github.com/junegunn/vim-plug) as plugin manager.
It is recommanded to read other vim resource and find useful plugins by yourself.

#### [Zathura](https://git.pwmt.org/pwmt/zathura)

Zathura is a minimalistic document viewer.
It use vi-like keybinding and suitable for many command line use.
This configuration only contain a theme which match global color scheme.
To install this plugin, install Zathura and copy or link zathura directory in this repo to `~/.config/zathura`.
This should setup color theme properly.

</details>

### X11

The following configurations are X11 specific, not suitable for wayland environment. 
I am moving to wayland currently and X11 configuration might be out-of-date as time pass.
See next section for wayland configuration.

<details>
<summary>Configurations</summary>

#### [Awesome](https://github.com/awesomeWM/awesome)

Awesome is a highly configurable tile window manager.
You should try cofigure your awesome by yourself if you decide to use it.
You can start from [ArchWiki](https://wiki.archlinux.org/title/Awesome) page.
If you want to adapt my template, you can put awesome directory this repo provided to `~/.config/awesome`.
To make it actually works, you need to do the following:

- Copy default theme to the user themes location:

    ```
    cp -r /usr/share/awesome/themes/default ~/.config/awesome/themes
    ```

    I use some resources provided by default theme. So make sure default theme is place on correct location.

- Chose your background and icon:

    Chose a background you like and place it at `~/.config/awesome/themes/sidonia/background`.
    Also pick an distro icon and place it at `~/.config/awesome/themes/sidonia/icons/distro`.

- Get some awesome widgets:

    ```
    git clone https://github.com/streetturtle/awesome-wm-widgets.git ~/.config/awesome/awesome-wm-widgets
    git clone https://github.com/streetturtle/awesome-buttons ~/.config/awesome/awesome-buttons
    ```

    There are tons of widgets you can install.
    I use some of them as default.
    Check [awesome-wm-widget](https://github.com/streetturtle/awesome-wm-widgets) for detail.
    Note that volume widget have memory leakage bug.
    Use with causion.

- Check applications you use:

    `~/.config/awesome/applictions/autostart.lua` defines what applications will be started at the start of your desktop.
    Add or remove it to meed your need. Also, check `~/.config/awesome/applications/default.lua` to modify default application for keybinding.
    You may also want to check `~/.config/awesome/wm/hotkeys.lua` for system command, such as shutdown, reboot, etc.

- Start awesome and use `Super+Shift+/` to view keybinding and do some modifications if needed. 
    
    Keybindings are defined in corresponding file in `~/.config/awesome`.
    It is highly recommanded to change keybinding because the workflow is very different for everyone.

#### [Picom](https://github.com/yshui/picom)

Picom is a compositor for X, forked from Compton.
Install picom and copy picom.conf in picom directory to `~/.config/picom/picom.conf` and you are done.
Picom will create halo and transparent effect for your windows.

#### [Rofi](https://github.com/davatorium/rofi)

Rofi is a window switcher, application launcher and dmenu replacement.
Install rofi, copy rofi directory provided by repo to `~/.config/rofi` and hit `Super+R` (if you use default keybinding) then a launcher menu will show up in front of you.
The color theme is modified from solarized-alternative, one of default theme provided by rofi.

#### [Conky](https://github.com/brndnmtthws/conky)

Conky is a light-weight system monitor for X that displays any kind of information on your desktop.
There are many configurations can be found.
I use a configuration provided by [monochrome](https://github.com/ernesto1/monochrome).

#### [feh](https://github.com/derf/feh)

feh is a light-weight image viewer and well integrated with command line use case.
This configuration contains vi-like keybinding and color theme setup.
To use this configuration, install feh and copy or link feh directory in this repo to `~/.config/feh` then you are done, but you may want to check which keybinding is changed by checking `~/.config/feh/keys`.

</details>

### Wayland

<details>
<summary>Configurations</summary>

#### [Hyprland](https://github.com/hyprwm/Hyprland)

A dynamic tiling Wayland compositor with awesome appearance and animations.

#### [imv](https://sr.ht/~exec64/imv/)

The imv is light-weight image viewer intended for use with CLI and tiling window managers/composter.
It support X11 and wayland as feh replacement.

#### [waybar](https://github.com/Alexays/Waybar)

A wayland desktop menu bar for wlroots based compositors.

#### [eww](https://github.com/elkowar/eww)

A highly customizable desktop widget system for X11 and wayland.
I use it to build dashboard and powermenu.

#### [fuzzel](https://codeberg.org/dnkl/fuzzel)

The fuzzel is a application launcher and a dmenu replacement for wayland.

#### [dunst](https://github.com/dunst-project/dunst)

The dunst is a sample notification daemon.

</details>

