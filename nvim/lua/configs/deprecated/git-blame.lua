require("gitblame").setup({
    enabled = false
})

vim.api.nvim_create_user_command('Gitblame', function()
    require('gitblame').toggle()
end, {bang=true})

