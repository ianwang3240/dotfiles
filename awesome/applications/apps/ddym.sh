#!/bin/sh

# Run dym with rofi (dmenu).

if [ -z "$1" ]; then
    WORD="$(rofi -dmenu -p "Word")"
else
    WORD="$1"
fi

if [ ! -z $WORD ]; then
    dym -c $WORD | rofi -dmenu -p dym | xclip -r -selection clipboard
fi

