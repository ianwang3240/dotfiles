return {
    setup = function(M, opts)
        M.builder = {}

        require('Project.build_system.configs').setup(
            M, opts
        )

        require('Project.build_system.build').setup(
            M, opts
        )
    end
}

