local profile = {}

profile.set_profile = function(fields)
    profile.fields = vim.tbl_deep_extend('force', profile.fields, fields)
end

profile.expand_header = function(name, vars)
    header = profile.headers.header(name)
    prefix = profile.headers.prefix(vim.fn.expand('%:e'))

    if header ~= nil then
        expanded = {}
        
        -- Add predefine vars.
        vars = vim.tbl_extend('force', profile.fields, vars or {})
        vars['file_name'] = vim.fn.expand('%:t')
        vars['year'] = os.date('%Y')
        vars['month'] = os.date('%m')
        vars['day'] = os.date('%d')

        for idx, line in ipairs(header) do
            expanded[idx] = prefix..string.gsub(line, "%${([%w_-]+)}", vars)
        end

        return expanded
    end

    return {}
end

profile.insert_header = function(name, vars)
    vim.api.nvim_buf_set_lines(
        0, 0, 0, false, profile.expand_header(name, vars)
    )
end

local setup = function(M, opts)
    M.profile = profile
    M.profile.fields = opts['profile'] or {}

    require('Project.profile.headers').setup(
        M, opts
    )

    -- LuaSnip support.
    local luasnip = require("luasnip")

    local snip = luasnip.snippet
    local text = luasnip.text_node
    local func = luasnip.function_node
    local insert = luasnip.insert_node

    for name, header in pairs(profile.headers.headers) do
        luasnip.add_snippets(nil, {
            all = {
                snip({
                    trig = "license."..name,
                    name = name.." License",
                    dscr = "License header."
                }, {
                    func(function()
                        return profile.expand_header(name)
                    end, {}),
                    text({"", ""}), insert(0)
                }) 
            }
        })
    end

    M.register_command('profile', {
        subcommands = {
            set = {
                execute = function(fargs)
                    field = table.remove(fargs, 1)
                    value = table.concat(fargs, ' ')

                    require('Project').profile.set_profile({
                        [field] = value
                    })
                end,
                complete = function()
                    return {
                        -- Provide some often used options.
                        'user_name',
                        'package_name',
                        'email'
                    }
                end
            }
        }
    })
    
    M.register_command('header', {
        subcommands = {
            insert = {
                execute = function(fargs)
                    require('Project').profile.insert_header(args.args)
                end,
                complete = function()
                    return vim.tbl_keys(
                        require('Project').profile.headers.headers
                    )
                end
            }
        }
    })
end

return {
    setup = setup
}
