require("applications.autostart")
require("applications.notifications")

local default = require("applications.default")
local hotkeys = require("applications.hotkeys")

return {
    default = default,
    hotkeys = hotkeys
}

