local awful = require("awful")
local gears = require("gears")
local naughty = require("naughty")

local default_apps = require("applications.default")

local standard = gears.table.join(
    -- Launch menu
    awful.key(
        { modkey, }, "r",
        function()
            awful.spawn("rofi -show combi")
        end,
        {description = "run launcher", group = "launcher"}
    ),

    -- Standard program
    -- Traditional hotkey for terminal in linux environment.
    -- awful.key(
    --     { modkey, }, "Return", 
    --     function () 
    --         awful.spawn(default_apps.terminal) 
    --     end,
    --     {description = "open a terminal", group = "launcher"}
    -- ),

    awful.key(
        { modkey, }, "x", 
        function () 
            awful.spawn(default_apps.terminal) 
        end, {
            description = "open a terminal", 
            group = "launcher"
        }
    ),

    awful.key(
        { modkey, "Shift" }, "f",
        function ()
            awful.spawn(default_apps.file_manager)
        end, { 
            description = "open file manager",
            group = "launcher"
        }
    ),

    awful.key(
        { modkey, }, "b",
        function()
            awful.spawn(default_apps.browser)
        end, {
            description = "open browser",
            group = "launcher"
        }
    ),

    awful.key(
        { modkey, }, "c",
        function()
            awful.spawn(default_apps.chat)
        end, {
            description = "open chat",
            group = "launcher"
        }
    ),

    awful.key(
        { modkey, }, "n",
        function()
            awful.spawn(default_apps.calculator)
        end, {
            description = "open calculator",
            group = "launcher"
        }
    ),

    awful.key(
        { modkey, }, "y",
        function()
            awful.spawn(default_apps.spelling)
        end, {
            description = "spell correction",
            group = "launcher"
        }
    ),

    awful.key(
        { modkey, }, "e",
        function()
            awful.spawn(default_apps.editor)
        end, {
            description = "open editor",
            group = "launcher"
        }
    )
)

-- Keymap for XF86 keys.
-- Note that most of them are comment out
-- because not all XF86 key have a matched HID keycode.
local xf86 = gears.table.join(
    -- awful.key(
    --     {}, "XF86WWW",
    --     function()
    --         awful.spawn(default_apps.browser)
    --     end, {
    --         description = "open browser",
    --         group = "launcher"
    --     }
    -- ),
    
    -- awful.key(
    --     {}, "XF86Terminal",
    --     function()
    --         awful.spawn(default_apps.terminal)
    --     end, {
    --         description = "open terminal",
    --         group = "launcher"
    --     }
    -- ),

    -- awful.key(
    --     {}, "XF86Messager",
    --     function()
    --         awful.spawn(default_apps.chat)
    --     end, {
    --         description = "open chat",
    --         group = "launcher"
    --     }
    -- ),

    -- awful.key(
    --     {}, "XF86Memo",
    --     function()
    --         awful.spawn(default_apps.notes)
    --     end, {
    --         description = "open notes",
    --         group = "launcher"
    --     }
    -- ),

    -- awful.key(
    --     {}, "XF86Documents",
    --     function()
    --         awful.spawn(default_apps.file_manager)
    --     end, {
    --         description = "open file manager",
    --         group = "launcher"
    --     }
    -- ),

    -- awful.key(
    --     {}, "XF86Word",
    --     function()
    --         awful.spawn(default_apps.spelling)
    --     end, {
    --         description = "spell correction",
    --         group = "launcher"
    --     }
    -- ),

    -- awful.key(
    --     {}, "XF86Music",
    --     function()
    --         awful.spawn(default_apps.music)
    --     end, {
    --         description = "open music player",
    --         group = "launcher"
    --     }
    -- ),

    -- awful.key(
    --     {}, "XF86Video",
    --     function()
    --         awful.spawn(default_apps.video)
    --     end, {
    --         description = "open video player",
    --         group = "launcher"
    --     }
    -- )
    
    awful.key(
        {}, "XF86Calculator",
        function()
            awful.spawn(default_apps.calculator)
        end, {
            description = "open calculator",
            group = "launcher"
        }
    ),

    awful.key(
        {}, "XF86Mail",
        function()
            awful.spawn(default_apps.mail)
        end, {
            description = "open email client",
            group = "launcher"
        }
    )
)

-- Special keys.
-- Key names are unknown.
-- Bindings are based on keycodes.
-- local special = gears.table.join()

return gears.table.join(
    standard, xf86, special
)
