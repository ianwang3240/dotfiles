" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Theme.
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" File manager.
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

" Autocompletion.
Plug 'Shougo/ddc.vim'
Plug 'vim-denops/denops.vim'

" Source.
Plug 'Shougo/ddc-around'
Plug 'shun/ddc-vim-lsp'

" Filters.
Plug 'Shougo/ddc-matcher_head'
Plug 'Shougo/ddc-sorter_rank'

" Syntex checking.
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'dense-analysis/ale'
Plug 'rhysd/vim-lsp-ale'

" Git
Plug 'airblade/vim-gitgutter'

" Initialize plugin system
call plug#end()

let g:airline_powerline_fonts = 1
let g:airline_theme='deus'

" NERDTree settins.
inoremap <silent> <F2> <C-o>:exec 'NERDTreeFocusToggle' <CR>
nnoremap <silent> <F2> :exec 'NERDTreeFocusToggle' <CR>

inoremap <silent> [1;5Q <C-o>:exec 'NERDTreeTabsToggle' <CR>
nnoremap <silent> [1;5Q :exec 'NERDTreeTabsToggle' <CR>

if has('macunix')
    nnoremap <silent> <C-Left> :exec 'tabprev' <CR>
    nnoremap <silent> <C-Right> :exec 'tabnext' <CR>
else
    nnoremap <silent> <C-Left> :exec 'tabprev' <CR>
    nnoremap <silent> <C-Right> :exec 'tabnext' <CR>
endif

let NERDTreeShowHidden=1

" Plugin specific settings.
" Easy to disable if plugin is removed.
source ~/.vim/custom/ddc.vim
source ~/.vim/custom/ale.vim
source ~/.vim/custom/vim-lsp.vim

" Vanilla vim settings.
syntax on

set expandtab
set tabstop=4
set shiftwidth=4

set number relativenumber

set autoindent

set backspace=indent,eol,start

set signcolumn=yes
highlight SignColumn ctermbg=None

" Desktop integrations.
if has("macunix")
    command Clipboard :call system('pbcopy', @0)
else
    command Clipboard :call system('xclip -selection c', @0)
end

