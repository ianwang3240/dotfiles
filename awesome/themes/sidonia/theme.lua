---------------------------
-- Default awesome theme --
---------------------------
local gears = require("gears")
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gfs = require("gears.filesystem")
local themes_path = os.getenv("HOME").."/.config/awesome/themes/"
local theme_name = "sidonia"

local theme = {}

theme.font          = "MesloLGS NF Bold 8"

theme.bg_normal     = "#222222"
theme.bg_focus      = "#535d6c"
theme.bg_urgent     = "#222222"
theme.bg_minimize   = "#222222"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#00aaff"
theme.fg_focus      = "#00aaff"
theme.fg_urgent     = "#ff0000"
theme.fg_minimize   = "#00cc00"

-- theme.useless_gap   = dpi(0.5)
theme.useless_gap   = dpi(5.0)
theme.border_width  = dpi(0.5)
theme.border_normal = "#00aaff"
theme.border_focus  = "#00aaff"
theme.border_urgent = "#ff0000"
theme.border_marked = "#00aaff"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

theme.hotkeys_fg                            = "#00aaff"
theme.hotkeys_modifiers_fg                  = "#aaaaaa"

theme.taglist_bg                            = "#00000000"
theme.taglist_bg_focus                      = "#00000000"
theme.taglist_bg_urgent                     = "#00000000"
theme.taglist_fg_urgent                     = "#ff0000"
theme.taglist_shape                         = gears.shape.rectangle
theme.taglist_shape_urgent                  = gears.shape.rectangle
theme.taglist_shape_border_width            = dpi(0)
theme.taglist_shape_border_width_focus      = dpi(0.5)
theme.taglist_shape_border_width_urgent     = dpi(0.5)
theme.taglist_shape_border_color            = "#00aaff"
theme.taglist_shape_border_color_focus      = "#00aaff"
theme.taglist_shape_border_color_urgent     = "#ff0000"

theme.tasklist_bg_normal                    = "#00000000"
theme.tasklist_bg_focus                     = "#00000000"
theme.tasklist_bg_minimize                  = "#00000000"
theme.tasklist_fg_minimize                  = "#00aaff"
theme.tasklist_fg_urgent                    = "#ff0000"
theme.tasklist_shape                        = gears.shape.rounded_rect
-- theme.tasklist_shape_minimized
theme.tasklist_shape_border_width           = dpi(0)
theme.tasklist_shape_border_width_minimized = dpi(2)
theme.tasklist_shape_border_width_focus     = dpi(2)
theme.tasklist_shape_border_width_urgent    = dpi(2)
theme.tasklist_shape_border_color           = "#000000"
theme.tasklist_shape_border_color_focus     = "#00aaff"
theme.tasklist_shape_border_color_minimized = "#004488"
theme.tasklist_shape_border_color_urgent    = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_focus
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]
theme.notification_font = "MesloLGS NF 12"
theme.notification_border_color = "#00aaff"
-- Fix icon size to avoid super large notification.
theme.notification_icon_size = 50

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path.."default/submenu.png"

theme.menu_font   = "MesloLGS NF Bold 12"
theme.menu_height = dpi(20)
theme.menu_width  = dpi(200)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = themes_path.."default/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = themes_path.."default/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = themes_path.."default/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = themes_path.."default/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = themes_path.."default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = themes_path.."default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path.."default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = themes_path.."default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path.."default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = themes_path.."default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path.."default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = themes_path.."default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path.."default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = themes_path.."default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path.."default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = themes_path.."default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = themes_path.."default/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = themes_path.."default/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = themes_path.."default/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = themes_path.."default/titlebar/maximized_focus_active.png"

theme.wallpaper = string.format(
    "%s/%s/background", themes_path, theme_name
)

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path.."default/layouts/fairhw.png"
theme.layout_fairv = themes_path.."default/layouts/fairvw.png"
theme.layout_floating  = themes_path.."default/layouts/floatingw.png"
theme.layout_magnifier = themes_path.."default/layouts/magnifierw.png"
theme.layout_max = themes_path.."default/layouts/maxw.png"
theme.layout_fullscreen = themes_path.."default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path.."default/layouts/tilebottomw.png"
theme.layout_tileleft   = themes_path.."default/layouts/tileleftw.png"
theme.layout_tile = themes_path.."default/layouts/tilew.png"
theme.layout_tiletop = themes_path.."default/layouts/tiletopw.png"
theme.layout_spiral  = themes_path.."default/layouts/spiralw.png"
theme.layout_dwindle = themes_path.."default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path.."default/layouts/cornernww.png"
theme.layout_cornerne = themes_path.."default/layouts/cornernew.png"
theme.layout_cornersw = themes_path.."default/layouts/cornersww.png"
theme.layout_cornerse = themes_path.."default/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
-- theme.icon_theme = /usr/share/icons

-- Additional icons.
theme.distro_icon = string.format(
    "%s/%s/icons/distro", themes_path, theme_name
)

return theme

