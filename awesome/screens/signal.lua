local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local naughty = require("naughty")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local focused = awful.screen.focused()

client.connect_signal("focus", function(c)
    if focused ~= c.screen then
        screen.emit_signal("property::focus", c.screen)
    end
end)

awful.screen.connect_for_each_screen(function(s)
    s.indicator = awful.popup({
        widget=wibox.widget {
            markup = '  ',
            align = 'center',
            valign = 'center',
            font = "MesloLGF NF 48",
            widget = wibox.widget.textbox,
        },
        border_width = 5,
        border_color = "#00aaff",
        screen = s,
        ontop = true,
        visible = false,
        placement = function(c)
            awful.placement.bottom(c, {
                offset = {
                    y = -dpi(50)
                }
            })
        end
    })
end)

screen.connect_signal("property::focus",
    function(s)
        -- if mouse.screen ~= c.screen then
            -- Oftenly, mouse is on another screen.
            -- Drag it back.
            -- awful.screen.focus(c.screen)
        -- end
        
        if s ~= focused then
            focused = s
        end
        
        for _, target in ipairs(screen) do
            local indicator = target.indicator
            
            if focused == target then
                indicator.visible = true
            
                if indicator.timer then
                    indicator.timer:stop()
                end

                indicator.timer = gears.timer({
                    timeout = 1
                })
                indicator.timer:connect_signal("timeout", function()
                    indicator.visible = false
                    timer:stop()
                end)
                indicator.timer:start()
            else
                indicator.visible = false
            end
        end
    end
)
