local awful = require("awful")
local gears = require("gears")
local arrows = require("arrows")

-- Focus client.
local focus_hotkeys = gears.table.join(
    -- Tab key to focus by index.
    awful.key({ modkey, }, "Tab",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),

    awful.key({ modkey, "Shift" }, "Tab",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    
    -- Arrow keys to focus by direction.
    -- Use arrow keys.
    arrows.key({modkey,}, "Left",
        function ()
            awful.client.focus.bydirection("left")
        end,
        {description = "focus left", group = "client"}
    ),

    arrows.key({modkey,}, "Right",
        function ()
            awful.client.focus.bydirection("right")
        end,
        {description = "focus right", group = "client"}
    ),

    arrows.key({modkey,}, "Up",
        function ()
            awful.client.focus.bydirection("up")
        end,
        {description = "focus up", group = "client"}
    ),

    arrows.key({modkey,}, "Down",
        function ()
            awful.client.focus.bydirection("down")
        end,
        {description = "focus down", group = "client"}
    ),

    awful.key({modkey, "Control"}, "Tab",
        function()
            awful.client.focus.history.previous()
        end,
        {description = "focus previous", group = "client"}
    )
)

-- Swap clients.
local swap_hotkeys = gears.table.join(
     -- By arrow keys.
    arrows.key(
        { modkey, "Shift"   }, "Right", 
        function() 
            awful.client.swap.bydirection("right")
        end,
        {description = "swap with right client", group = "client"}
    ),
    
    arrows.key(
        { modkey, "Shift"   }, "Left", 
        function() 
            awful.client.swap.bydirection("left") 
        end,
        {description = "swap with left client", group = "client"}
    ),

    arrows.key(
        { modkey, "Shift"   }, "Up", 
        function() 
            awful.client.swap.bydirection("up")
        end,
        {description = "swap with up client", group = "client"}
    ),
    
    arrows.key(
        { modkey, "Shift"   }, "Down", 
        function() 
            awful.client.swap.bydirection("down") 
        end,
        {description = "swap with down client", group = "client"}
    )
)

misc_hotkeys = gears.table.join(
    awful.key({modkey,}, "z", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),

    awful.key({modkey, "Control"}, "m",
          function ()
              local c = awful.client.restore()
              -- Focus restored client
              if c then
                c:emit_signal(
                    "request::activate", "key.unminimize", {raise = true}
                )
              end
          end,
          {description = "restore minimized", group = "client"}
    )
)

client_hotkeys = gears.table.join(
    awful.key(
        { modkey, }, "q",      
        function(c) 
            c:kill()                         
        end,
        {description = "close", group = "client"}
    ),
    
    awful.key(
        { modkey, }, "Return", 
        function(c) 
            c:swap(awful.client.getmaster()) 
        end,
        {description = "move to master", group = "client"}
    ),

    -- awful.key(
    --     { modkey, "Shift"}, "]",      
    --     function(c) 
    --         c:move_to_screen(c.screen.index+1)               
    --     end,
    --     {description = "move to next screen", group = "client"}
    -- ),

    -- awful.key(
    --     { modkey, "Shift"}, "[",
    --     function(c)
    --         c:move_to_screen(c.screen.index-1)
    --     end,
    --     {description = "move to previous screen", group = "client"}
    -- ),
    
    awful.key({modkey, }, "m",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}
    ),
    
    awful.key({modkey, 'Shift'}, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "(un)maximize", group = "client"}
    ),

    awful.key({ modkey, }, "f", 
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}
    )
)

return {
    global = gears.table.join(
        focus_hotkeys,
        swap_hotkeys,
        misc_hotkeys
    ),
    client = gears.table.join(
        client_hotkeys
    )
}
