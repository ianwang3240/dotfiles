local awful = require("awful")

-- Home directory.
HOME = os.getenv("HOME")

-- Application location.
Apps = HOME.."/.config/awesome/applications/apps"

-- Autostart.

-- Screen saver and power management.
-- awful.util.spawn("xfce4-power-manager")
awful.util.spawn(Apps.."/idlehook.sh")

-- Window effect and desktop widgets.
awful.spawn.once("picom")
awful.spawn(HOME.."/.config/conky/conky.sh")

-- Bluetooth.
-- awful.spawn.once("nm-applet")
-- awful.spawn.once("blueman-applet")

-- Other user applications.
awful.spawn.once("keepassxc")
awful.spawn.once("nextcloud")

