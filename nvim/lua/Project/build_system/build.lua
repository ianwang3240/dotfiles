notify = require(
    'Project.build_system.notify'
)

-- Check if quickfix list containes an error.
local has_error = function(qflist)
    if qflist == nil then
        return true
    end

    for idx, item in ipairs(qflist) do
        -- Ignore warnings
        if item['valid'] == 1 and (
            item['type'] == 'E' or item['type'] == 'e'
        ) then
            return true
        end
    end

    return false
end

-- Get execution command.
local eval_execution_command = function(cmd)
    cmd = string.gsub(cmd, "%${([%w_-]+)}", {
        file_name = '%',
        target = require('Project').builder['target'],
        -- project_name
        -- project_root
        -- src_root
        -- build_type (test, debug, release)
    })

    return cmd
end

local setup = function(M, opts)
    builder = M.builder

    builder.run_command = opts.run

    builder.qflist = opts.qflist

    -- Build executable target.
    builder.build = function(config_name)
        vim.cmd('stopinsert')

        if builder.build_system_id == nil then
            notify.error(
                'Project build system not config!'
            )
            return
        end

        local config_name = config_name or 'release'
        local config = builder.configs[config_name]
        
        if config == nil then
            notify.error('Invalid config: %s', config_name)
            return
        end

        notify.info('Build with %s config', config_name)

        -- Explicitly save all current buffers before compiling.
        vim.cmd[[wa]]

        vim.opt.makeprg = eval_execution_command(
            config.build
        )

        vim.opt.errorformat = builder.errorformat

        -- Build target
        vim.cmd [[
            make! | redraw
        ]]

        -- Check build result
        if has_error(
            vim.fn.getqflist()
        ) then
            builder.qflist()
            
            return false
        else
            return true
        end
    end

    -- Run executable target.
    builder.run = function(config_name)
        vim.cmd('stopinsert')

        if builder.build_system_id == nil then
            notify.error(
                'Project build system not config!'
            )
            return
        end

        local config_name = config_name or 'release'
        local config = builder.configs[config_name]

        if config == nil then
            notify.error('Invalid config: %s', config_name)
            return
        end

        notify.info('Run with %s config', config_name)

        if config['run'] ~= nil then
            -- Run command.
            builder.run_command(
                vim.fn.expandcmd(
                    eval_execution_command(
                        config['run']
                    )
                )
            )
        end
    end

    -- Build and run.
    builder.build_and_run = function(config_name)
        if builder.build_system_id == nil then
            notify.error(
                'Project build system not config!'
            )
            return
        end

        local config_name = config_name or 'release'
        
        if builder.build(config_name) then
            builder.run(config_name)
        end
    end

    M.register_command('builder', {
        subcommands = {
            build = {
                execute = function(fargs)
                    local config = table.concat(fargs)
                    if config == '' then
                        config = nil
                    end

                    require('Project').builder.build(
                        config
                    )
                end,
                complete = function(fargs)
                    return {
                        'release', 
                        'debug'
                    }
                end
            },
            run = {
                execute = function(fargs)
                    local config = table.concat(fargs)
                    if config == '' then
                        config = nil
                    end

                    require('Project').builder.run(
                        config
                    )
                end
            },
            build_and_run = {
                execute = function(fargs)
                    local config = table.concat(fargs)
                    if config == '' then
                        config = nil
                    end

                    require('Project').builder.build_and_run(
                        config
                    )
                end,
                complete = function(fargs)
                    return {
                        'release', 
                        'debug'
                    }
                end
            }
        }
    })
end

return {
    setup = setup
}
