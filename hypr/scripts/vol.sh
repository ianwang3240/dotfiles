#!/bin/bash

if [ "$1" == "inc" ]; then
    wpctl set-volume -l 1.0 @DEFAULT_SINK@ 5%+
elif [ "$1" == "dec" ]; then
    wpctl set-volume @DEFAULT_SINK@ 5%-
elif [ "$1" == "mute" ]; then
    # Toggle mute
    wpctl set-mute @DEFAULT_SINK@ toggle
fi
