require('telescope').setup {
    defaults = {
        sorting_strategy = "ascending",
        initial_mode = 'normal',
        layout_strategy = 'bottom_pane'
    }
}

vim.api.nvim_create_user_command(
    'Registers', function()
        require("telescope.builtin").registers()
    end, {
        bang = true
    }
)

vim.api.nvim_create_user_command(
    'Todo', function()
        require("telescope").load_extension('todo-comments').todo()
    end, {
        bang = true
    }
)

vim.keymap.set(
    'n', '<leader>ff', function()
        require("telescope.builtin").find_files({
            initial_mode = "insert"
        })
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', '<leader>fg', function()
        require("telescope.builtin").live_grep({
            initial_mode = "insert"
        })
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', '<leader>fs', function()
        require("telescope.builtin").lsp_document_symbols()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', '<leader>ws', function()
        require("telescope.builtin").lsp_workspace_symbols()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', '<leader>ls', function()
        require("telescope.builtin").buffers()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', '<leader>er', function()
        require("telescope.builtin").diagnostics()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', 'gd', function()
        require("telescope.builtin").lsp_definitions()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', 'gi', function()
        require("telescope.builtin").lsp_implementions()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    'n', 'gc', function()
        require("telescope.builtin").lsp_outgoing_calls()
    end, {silent = true, noremap = true}
)

