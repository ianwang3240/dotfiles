general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 5
    gaps_out = 20
    border_size = 2
    col.active_border = rgba(00aaffff)
    col.inactive_border = rgba(00000000)

    layout = dwindle
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    
    pseudotile = yes # master switch for pseudotiling
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
}

# dwindle
# bind = $mainMod, I, pseudo,
bind = $mainMod, Z, togglesplit,

# Groups
# bind = $mainMod, G, togglegroup,
# bind = $mainMod CTRL, Tab, changegroupactive, f
# bind = $mainMod, Tab, changegroupactive, f
# bind = $mainMod SHIFT, Tab, changegroupactive, b
# bind = $mainMod, bracketright, changegroupactive, f
# bind = $mainMod, bracketleft, changegroupactive, b

misc {
    focus_on_activate = true
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 10

    blur {
        enabled = true
        size = 3
        passes = 1
        new_optimizations = on
    }

    drop_shadow = yes
    shadow_range = 20
    shadow_render_power = 3
    col.shadow = rgba(00aaffee)
    col.shadow_inactive = rgba(00000000)

    active_opacity = 1.0
    inactive_opacity = 0.8
    fullscreen_opacity = 1.0
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = easeOut, 0.05, 1.0, 0.0, 1.0

    animation = windows, 1, 2, easeOut
    animation = windowsOut, 1, 2, default, popin 80%
    animation = border, 1, 3, default
    animation = fade, 1, 2, default
    animation = workspaces, 1, 2, default
}

# Window rules
#
# Example windowrule v1
# windowrule = float, ^(kitty)$
#
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
#
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more

# KeePassXC database unlocking prompt
windowrulev2 = float, class:^(org.keepassxc.KeePassXC)$,title:^(Unlock)
# Make FreeTube, LBRY, and browser opaque while playing video
windowrulev2 = opaque, class:^(FreeTube)$
windowrulev2 = opaque, class:^(LBRY)$
windowrulev2 = opaque, title:^((.+)\[(電影|\d+)\] (\[.+\] )?線上看 (-|—) 巴哈姆特動畫瘋 (- Brave|— LibreWolf))$
windowrulev2 = opaque, class: ^(LibreWolf)$, title:^(Picture-in-Picture)$

# Bindings
bind = $mainMod, M, togglefloating,
bind = $mainMod, M, centerwindow,
bind = $mainMod, F, fullscreen, 0
bind = $mainMod, G, fullscreen, 1
bind = $mainMod, Q, killactive, 

bind = $mainMod, Tab, cyclenext
bind = $mainMod SHIFT, Tab, cyclenext, prev

# Move focus with mainMod
bind = $mainMod, L, movefocus, r
bind = $mainMod, H, movefocus, l
bind = $mainMod, K, movefocus, u
bind = $mainMod, J, movefocus, d

bind = $mainMod, D, movefocus, r
bind = $mainMod, A, movefocus, l
bind = $mainMod, W, movefocus, u
bind = $mainMod, S, movefocus, d

# Move window with mainMod + shift
bind = $mainMod SHIFT, L, movewindow, r
bind = $mainMod SHIFT, H, movewindow, l
bind = $mainMod SHIFT, K, movewindow, u
bind = $mainMod SHIFT, J, movewindow, d

bind = $mainMod SHIFT, D, movewindow, r
bind = $mainMod SHIFT, A, movewindow, l
bind = $mainMod SHIFT, W, movewindow, u
bind = $mainMod SHIFT, S, movewindow, d

# Swap window with mainMod + alt + shift
bind = $mainMod SHIFT ALT, L, swapwindow, r
bind = $mainMod SHIFT ALT, H, swapwindow, l
bind = $mainMod SHIFT ALT, K, swapwindow, u
bind = $mainMod SHIFT ALT, J, swapwindow, d

bind = $mainMod SHIFT ALT, D, movewindow, r
bind = $mainMod SHIFT ALT, A, movewindow, l
bind = $mainMod SHIFT ALT, W, movewindow, u
bind = $mainMod SHIFT ALT, S, movewindow, d

# Move active window to a workspace with mainMod + SHIFT + [1-9]
bind = $mainMod SHIFT, 1, movetoworkspace, 1
bind = $mainMod SHIFT, 2, movetoworkspace, 2
bind = $mainMod SHIFT, 3, movetoworkspace, 3
bind = $mainMod SHIFT, 4, movetoworkspace, 4
bind = $mainMod SHIFT, 5, movetoworkspace, 5
bind = $mainMod SHIFT, 6, movetoworkspace, 6
bind = $mainMod SHIFT, 7, movetoworkspace, 7
bind = $mainMod SHIFT, 8, movetoworkspace, 8
bind = $mainMod SHIFT, 9, movetoworkspace, 9
# Move active window to a workspace with mainMod + SHIFT + [F1-F3]
bind = $mainMod SHIFT, F1, movetoworkspace, 7
bind = $mainMod SHIFT, F2, movetoworkspace, 8
bind = $mainMod SHIFT, F3, movetoworkspace, 9

# Move active window to a workspace with mainMod + ALT + SHIFT + [1-9] silently
bind = $mainMod SHIFT ALT, 1, movetoworkspacesilent, 1
bind = $mainMod SHIFT ALT, 2, movetoworkspacesilent, 2
bind = $mainMod SHIFT ALT, 3, movetoworkspacesilent, 3
bind = $mainMod SHIFT ALT, 4, movetoworkspacesilent, 4
bind = $mainMod SHIFT ALT, 5, movetoworkspacesilent, 5
bind = $mainMod SHIFT ALT, 6, movetoworkspacesilent, 6
bind = $mainMod SHIFT ALT, 7, movetoworkspacesilent, 7
bind = $mainMod SHIFT ALT, 8, movetoworkspacesilent, 8
bind = $mainMod SHIFT ALT, 9, movetoworkspacesilent, 9
# Move active window to a workspace with mainMod + ALT + SHIFT + [F1-F3] silently
bind = $mainMod SHIFT ALT, F1, movetoworkspacesilent, 7
bind = $mainMod SHIFT ALT, F2, movetoworkspacesilent, 8
bind = $mainMod SHIFT ALT, F3, movetoworkspacesilent, 9

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

