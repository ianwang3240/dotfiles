local apps = {}

local HOME = os.getenv("HOME")
local APPS = HOME.."/.config/awesome/applications/apps"

-- Terminal
apps.terminal = "alacritty"
apps.cli = function(cmd)
    return string.format(
        "%s -e %s", apps.terminal, cmd
    )
end

-- Editor
apps.editor = apps.cli(
    os.getenv("EDITOR") or "nvim"
)
apps.edit = function(file)
    return string.format("%s %s", apps.editor, file)
end

-- GUI file manager
apps.file_manager = "thunar"

-- Browser
apps.browser = "librewolf"
apps.browser_window_class = "librewolf"
apps.browser_new_tab = function(url)
    return string.format("%s --new-tab '%s'", apps.browser, url)
end
apps.browser_new_default_tab = apps.browser_new_tab(
    "about:newtab"
)

-- Screen locker
apps.screen_locker = APPS.."/i3lock-color.sh"

-- Chat software.
apps.chat = "nheko"

-- Calculator.
apps.calculator = apps.cli("bc -l")

-- Notes.
apps.notes = apps.cli(string.format(
    "nvim %s/Nextcloud/Notes/index.norg", HOME
))

-- Spell correction.
apps.spelling = APPS.."/ddym.sh"

-- Email.
apps.mail = apps.cli("neomutt")

-- Music.
apps.music = "tauon-music-box"

-- Video
apps.video = "lbry"

return apps

