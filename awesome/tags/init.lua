hotkeys = require("tags.hotkeys")
taglist = require("tags.taglist")

return {
    hotkeys = hotkeys,
    taglist = taglist
}
