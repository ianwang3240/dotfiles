#!/bin/bash

# Get all display from compositor (hyprland in this case)
# displays=$(hyprctl monitors -j | jq -r ".[].name")

display=$1

# Output save location
path=$HOME/Pictures/Screenshots

if [ -z "$2" ]; then
    filename=$3
else
    # Get timestamp, which will be used as part of output name
    timestamp=$(date "+%Y%m%d_%Hh%Mm%Ss%N")

    # Include timestamp and display name in output file name
    filename=screenshot_${timestamp}_${display}.png
fi

# Do capturing
grim -o $display $path/$filename

# Send notification
action=$(dunstify "Screenshot" "A screenshot have saved to ${filename}" \
    --timeout=3000 \
    --action="default, Open" \
)

case "${action}" in
"default")
    thunar $path/$filename
    ;;
esac

