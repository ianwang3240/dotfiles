local awful = require("awful")
local gears = require("gears")
local arrows = require("arrows")

local hotkeys = gears.table.join(
    awful.key(
        {modkey, "Shift"}, "`",   
        awful.tag.viewprev,
        {description = "view previous tag", group = "tag"}
    ),
    
    awful.key(
        {modkey, }, "`", 
        awful.tag.viewnext,
        {description = "view next tag", group = "tag"}
    ),  
 
    awful.key(
        {modkey, }, "'", 
        awful.tag.history.restore,
        {description = "back to last viewed tag", group = "tag"}
    )
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    hotkeys = gears.table.join(hotkeys,
        -- View tag only.
        awful.key(
            { modkey }, "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                   tag:view_only()
                end
            end,
            {description = "view tag #"..i, group = "tag"}
        ),
        
        -- Toggle tag display.
        awful.key(
            { modkey, "Control" }, "#" .. i + 9,
            function ()
                local screen = awful.screen.focused()
                local tag = screen.tags[i]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
            {description = "toggle tag #" .. i, group = "tag"}
        ),
        
        -- Move client to tag.
        awful.key(
            { modkey, "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
            {description = "move focused client to tag #"..i, group = "tag"}
        ),
        
        -- Toggle tag on focused client.
        awful.key(
            { modkey, "Control", "Shift" }, "#" .. i + 9,
            function ()
                if client.focus then
                    local tag = client.focus.screen.tags[i]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
            {description = "toggle focused client on tag #" .. i, group = "tag"}
        )
    )
end

return hotkeys

