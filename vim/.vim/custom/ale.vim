let g:ale_sign_column_always = 1
let g:ale_floating_preview = 1
let g:ale_hover_to_floating_preview = 1

augroup ale_hover_cursor
  autocmd!
  autocmd CursorHold * ALEHover
augroup END

let g:ale_sign_error = '>'
let g:ale_sign_warning = '>'

nnoremap <silent> gd :exec 'ALEGoToDefinition' <CR>

