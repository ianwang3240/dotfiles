#!/bin/bash

if [ "$1" == "play-pause" ]; then
    playerctl play-pause 
elif [ "$1" == "next" ]; then
    playerctl next
elif [ "$1" == "previous" ]; then
    playerctl previous
fi

