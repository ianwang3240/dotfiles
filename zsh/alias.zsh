function n {
    if [ -z "$1" ]; then
        alacritty --working-directory="$(pwd)" &> /dev/null &!
    else
        alacritty --working-directory="$1" &> /dev/null &!
    fi
}

# File manager
function vifm {
    local dst="$(command vifm --choose-dir - "$@")"
    if [ -z "$dst" ]; then
        echo 'Directory picking cancelled/failed'
        return 1
    fi
    cd "$dst"
}

alias lf="vifm"
alias fm="vifm"

# Clipboard
if [[ "$OSTYPE" == "darwin"* ]]; then
    alias cb="pbcopy"
else
    # xclip global clipboard.
    alias cb="xclip -selection clipboard"
fi

# Notes
alias notes="nvim ~/Nextcloud/Notes/index.norg"

# Calculator.
function calc {
    echo $1 | bc -l
}

# Calendar
alias ical="ikhal"

# Contacts
alias contacts="khard"

# Calendar and contacts sync
alias csync="vdirsyncer discover && vdirsyncer sync"

# Torrent
alias torrent="transmission-remote"

# Linux specific
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Open file.
    function open {
        xdg-open $1 &!
    }

    # Open file as pdf using pandoc.
    function pdf {
        pandoc --pdf-engine=xelatex -t pdf $1 -o - -V mainfont="Source Han Sans HC" | zathura -
    }

    # gvfs trash functionality.
    alias trash="gio trash"

    # Online dictionary
    # function dict {
    #     curl dict.org/d:$1
    # }

    # Gnome dictionary GUI
    alias dict="gnome-dictionary --look-up"
fi

