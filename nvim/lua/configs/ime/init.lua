vim.g.ime_plugins = {
    'zhuyin', 'builtin_kana', 'wide', 'runes'
}

vim.g.ime_toggle_english = '<C-Space>'
vim.g.ime_select_mode = '<F11>'
vim.g.ime_switch_2nd = '<M-Space>'
vim.g.ime_menu = '<F12>'

vim.keymap.set(
    {'i', 'n'}, '<C-Space>', function()
        vim.fn['ime#toggle_english']()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    {'i', 'n'}, '<F11>', function()
        vim.fn['ime#plugin_menu']()
    end, {silent = true, noremap = true}
)

vim.keymap.set(
    {'i', 'n'}, '<F12>', function()
        vim.fn['ime#_mode_menu_window']()
    end, {silent = true, noremap = true}
)

