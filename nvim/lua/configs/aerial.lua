local aerial = require('aerial')

aerial.setup({
    keymaps = {
        ["<esc>"] = "actions.close",
        ["l"] = function()
            aerial.tree_open()
            aerial.select()
            aerial.close()
        end,
        ["<CR>"] = function()
            aerial.select()
            aerial.close()
        end
    },
    layout = {
        max_width = 30,
        min_width = 30
    }
})

-- vim.keymap.set(
--     {'i', 'n'}, '<F3>',
--     function()
--         require('aerial').toggle({
--             direction='left'
--         })
--     end,
--     {silent = true, noremap = true}
-- )
 
-- vim.api.nvim_create_user_command('Symbols', function()
--      require('aerial').toggle({
--          direction='left'
--      })
-- end, {bang = true})
 
-- vim.api.nvim_create_user_command('CodeOutline', function()
--     require("telescope").extensions.aerial.aerial()
-- end, {bang = true})

