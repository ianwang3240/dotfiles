return {
    error = function(msg, ...)
        vim.notify(string.format(
            msg, ...
        ), vim.log.levels.ERROR, {
            title = 'Project Build System'
        })
    end,

    info = function(msg, ...)
        vim.notify(string.format(
            msg, ...
        ), vim.log.levels.INFO, {
            title = 'Project Build System'
        })
    end
}
