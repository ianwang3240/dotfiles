local awful = require("awful")

return function(theme, hotkeys, buttons)
    return {
        -- All clients will match this rule.
        { rule = { },
          properties = { 
            border_width = theme.border_width,
            border_color = theme.border_normal,
            focus = awful.client.focus.filter,
            raise = true,
            keys = hotkeys,
            buttons = buttons,
            screen = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen,
            size_hints_honor = false
         }
        },

        -- Floating clients.
        { rule_any = {
            instance = {
              "DTA",  -- Firefox addon DownThemAll.
              "copyq",  -- Includes session name in class.
              "pinentry",
            },
            class = {
              "Arandr",
              "Blueman-manager",
              "Gpick",
              "Kruler",
              "MessageWin",  -- kalarm.
              "Sxiv",
              "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
              "Wpa_gui",
              "veromix",
              "xtightvncviewer",
              "Nextcloud",
           },

            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name = {
              "Event Tester",  -- xev.
            },
            role = {
              "AlarmWindow",  -- Thunderbird's calendar.
              "ConfigManager",  -- Thunderbird's about:config.
              "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
            }
          }, properties = { floating = true }},

        -- Add titlebars to normal clients and dialogs
        { 
            rule_any = {
                type = { "normal", "dialog" }
            }, 
            properties = { 
                titlebars_enabled = false 
            }
        },

        { 
            rule = {
                class = "conky",
            },
            properties = {
                border_width = 0
            }
        },
        
        -- Password manager.
        -- {
        --     rule = {
        --         class = 'KeePassXC'
        --     },
        --     properties = {
        --         floating = true,
        --         raise = true,
        --         placement = awful.placement.centered
        --     }

        -- },
    }
end
